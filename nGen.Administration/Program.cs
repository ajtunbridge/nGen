﻿#region Using directives

using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Caching;
using nGen.Domain.Data;

#endregion

namespace nGen.Administration
{
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Session.Initialize();

            Application.Run(new MainForm());
        }
    }
}