﻿#region Using directives

using System;
using System.Drawing;
using System.Windows.Forms;
using nGen.Domain.Security;

#endregion

namespace nGen.Administration.Dialogs
{
    public partial class SetPasswordDialog : Form
    {
        private const PasswordStrength MinimumPasswordStrengthAllowed = PasswordStrength.NotBad;

        public SetPasswordDialog()
        {
            InitializeComponent();
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            okayButton.Enabled = false;

            passwordStrengthIndicator.ShowStrength(passwordTextBox.Text);

            if (passwordTextBox.Text != confirmPasswordTextBox.Text) {
                statusLabel.Text = "Passwords do not match!";
                statusLabel.ForeColor = Color.Firebrick;
            }
        }

        private void confirmPasswordTextBox_TextChanged(object sender, EventArgs e)
        {
            okayButton.Enabled = false;

            if (passwordTextBox.Text != confirmPasswordTextBox.Text) {
                statusLabel.Text = "Passwords do not match!";
                statusLabel.ForeColor = Color.Firebrick;
            }
            else {
                if (passwordStrengthIndicator.PasswordStrength < MinimumPasswordStrengthAllowed) {
                    statusLabel.Text = "Password doesn't meet requirements!";
                    statusLabel.ForeColor = Color.Firebrick;
                }
                else {
                    statusLabel.Text = "Password is okay!";
                    statusLabel.ForeColor = Color.Green;
                    okayButton.Enabled = true;
                }
            }
        }
    }
}