﻿#region Using directives

using nGen.Domain.Dialogs;
using nGen.Domain.IO;
using nGen.Domain.Security;
using Ninject.Modules;

#endregion

namespace nGen.Administration.Injection
{
    internal sealed class ServiceNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IPasswordService>().To<PBKDF2PasswordService>();
        }
    }
}