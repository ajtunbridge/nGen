﻿#region Using directives

using Ninject.Modules;

#endregion

namespace nGen.Administration.Injection
{
    public class MySQLDataNinjectModule : NinjectModule
    {
        public override void Load()
        {
            // TODO: setup Ninject bindings to nGen.Data.MySQL
        }
    }
}