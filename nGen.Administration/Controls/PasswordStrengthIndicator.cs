﻿#region Using directives

using System;
using System.Drawing;
using System.Windows.Forms;
using nGen.Domain.Security;

#endregion

namespace nGen.Administration.Controls
{
    public partial class PasswordStrengthIndicator : Control
    {
        private Color _foreColor = Color.Red;
        private int _fillPercentage = 0;
        private string _strength = "too weak";

        public PasswordStrength PasswordStrength { get; private set; }

        public PasswordStrengthIndicator()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            using (var fillBrush = new SolidBrush(_foreColor)) {
                var multiplier = (double) _fillPercentage/100;
                var rightFillPoint = Convert.ToInt32(Width*multiplier);
                var rect = new Rectangle(0, 0, rightFillPoint, Height);
                pe.Graphics.FillRectangle(fillBrush, rect);
            }

            // Create a StringFormat object with the each line of text, and the block 
            // of text centered on the page.
            var stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;

            using (var textBrush = new SolidBrush(Color.Black)) {
                pe.Graphics.DrawString(_strength, Font, textBrush, pe.ClipRectangle, stringFormat);
            }

            ControlPaint.DrawBorder3D(pe.Graphics, pe.ClipRectangle, Border3DStyle.Flat);
        }

        public void ShowStrength(string password)
        {
            PasswordStrength = PasswordAdvisor.CalculateStrength(password);

            switch (PasswordStrength)
            {
                case PasswordStrength.Pointless:
                    _foreColor = Color.Red;
                    _fillPercentage = 0;
                    _strength = "too weak";
                    break;
                case PasswordStrength.TooPopular:
                    _foreColor = Color.OrangeRed;
                    _fillPercentage = 15;
                    _strength = "too popular";
                    break;

                case PasswordStrength.Poor:                
                    _foreColor = Color.OrangeRed;
                    _fillPercentage = 30;
                    _strength = "poor";
                    break;
                case PasswordStrength.NotBad:
                    _foreColor = Color.Yellow;
                    _fillPercentage = 50;
                    _strength = "not too bad";
                    break;
                case PasswordStrength.PrettyGood:
                    _foreColor = Color.YellowGreen;
                    _fillPercentage = 75;
                    _strength = "pretty good";
                    break;
                case PasswordStrength.SuperPassword:
                    _foreColor = Color.LimeGreen;
                    _fillPercentage = 100;
                    _strength = "super password";
                    break;
            }

            Refresh();
        }
    }
}