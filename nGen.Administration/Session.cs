﻿#region Using directives

using nGen.Administration.Injection;
using Ninject;
using Ninject.Modules;

#endregion

namespace nGen.Administration
{
    internal static class Session
    {
        private static StandardKernel _kernel;

        internal static void Initialize()
        {
            // setup dependency injection
            _kernel = new StandardKernel(new INinjectModule[] {
                new EFDataNinjectModule(),
                new ServiceNinjectModule()
            });
        }

        internal static T GetInstance<T>()
        {
            return _kernel.Get<T>();
        }
    }
}