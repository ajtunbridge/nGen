﻿#region Using directives

using System.Collections.Generic;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Data.Repositories
{
    public interface IEmployeeRepository : IRepository<IEmployee>
    {
        IEmployee GetByUserName(string userName);

        ICollection<IEmployee> FilterByFirstName(string query);

        ICollection<IEmployee> FilterByLastName(string query);

        ICollection<IEmployee> FilterByEmployeeGroup(IEmployeeGroup employeeGroup);

        ICollection<IEmployee> FilterByEmployeeGroup(int employeeGroupId);
    }
}