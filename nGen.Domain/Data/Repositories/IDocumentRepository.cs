﻿#region Using directives

using System.Collections.Generic;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Data.Repositories
{
    public interface IDocumentRepository : IRepository<IDocument>
    {
        ICollection<IDocument> GetCustomerDocuments(ICustomer customer);

        ICollection<IDocument> GetFolderDocuments(IDocumentFolder documentFolder);

        ICollection<IDocument> GetPartDocuments(IPart part);

        ICollection<IDocument> GetPartVersionDocuments(IPartVersion partVersion);
    }
}