﻿#region Using directives

using System.Collections.Generic;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Data.Repositories
{
    public interface IDocumentVersionRepository : IRepository<IDocumentVersion>
    {
        /// <summary>
        ///     Get the most recently created <see cref="IDocumentVersion" /> for the specified <see cref="IDocument" />
        /// </summary>
        /// <param name="document">The <see cref="IDocument" /> to get the latest version of</param>
        /// <returns></returns>
        IDocumentVersion GetLatestVersion(IDocument document);

        ICollection<IDocumentVersion> GetAllVersions(IDocument document);
    }
}