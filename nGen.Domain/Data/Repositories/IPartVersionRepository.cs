﻿#region Using directives

using System.Collections.Generic;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Data.Repositories
{
    public interface IPartVersionRepository : IRepository<IPartVersion>
    {
        ICollection<IPartVersion> GetPartVersions(IPart part);
    }
}