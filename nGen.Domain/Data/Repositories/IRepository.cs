﻿#region Using directives

using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Data.Repositories
{
    public interface IRepository<T> where T : class, IEntity
    {
        /// <summary>
        ///     Get the record with the specified ID value
        /// </summary>
        /// <param name="id">The ID value of the record to get</param>
        /// <returns></returns>
        T GetByPK(int id);

        /// <summary>
        ///     Add the supplied entity to the database
        /// </summary>
        /// <param name="entity">The entity to add</param>
        void Add(T entity);

        /// <summary>
        ///     Update the supplied entity to the database
        /// </summary>
        /// <param name="entity">The entity to update</param>
        void Update(T entity);

        /// <summary>
        ///     Delete the supplied entity from the database
        /// </summary>
        /// <param name="entity">The entity to remove from the database</param>
        void Delete(T entity);
    }
}