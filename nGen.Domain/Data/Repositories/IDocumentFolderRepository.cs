﻿#region Using directives

using System.Collections.Generic;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Data.Repositories
{
    public interface IDocumentFolderRepository : IRepository<IDocumentFolder>
    {
        ICollection<IDocumentFolder> GetChildFolders(IDocumentFolder parentFolder);

        ICollection<IDocumentFolder> GetCustomerFolders(ICustomer customer);

        ICollection<IDocumentFolder> GetPartFolders(IPart part);

        ICollection<IDocumentFolder> GetPartVersionFolders(IPartVersion partVersion);
    }
}