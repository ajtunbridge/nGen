﻿#region Using directives

using System.Collections.Generic;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Data.Repositories
{
    public interface IEmployeeGroupRepository : IRepository<IEmployeeGroup>
    {
        ICollection<IEmployeeGroup> FilterByName(string searchPattern);
    }
}