﻿#region Using directives

using System.Collections.Generic;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Data.Repositories
{
    public interface IPartRepository : IRepository<IPart>
    {
        ICollection<IPart> GetCustomerParts(ICustomer customer);

        ICollection<IPart> GetCustomerParts(int customerId);
    }
}