﻿#region Using directives

using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Data.Repositories
{
    public interface IClientSettingRepository : IRepository<IClientSetting>
    {
        IClientSetting GetClientSettings();
    }
}