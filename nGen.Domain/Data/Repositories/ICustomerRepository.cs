﻿#region Using directives

using System.Collections.Generic;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Data.Repositories
{
    public interface ICustomerRepository : IRepository<ICustomer>
    {
        ICollection<ICustomer> FilterByName(string value);

        ICollection<ICustomer> GetAll(bool approvedOnly);
    }
}