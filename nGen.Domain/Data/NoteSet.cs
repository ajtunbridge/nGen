﻿#region Using directives

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

#endregion

namespace nGen.Domain.Data
{
    [Serializable]
    public class NoteSet : IEnumerable<Note>
    {
        private readonly HashSet<Note> _notes;

        public NoteSet()
        {
            _notes = new HashSet<Note>();
        }

        #region IEnumerable<Note> Members

        public IEnumerator<Note> GetEnumerator()
        {
            return _notes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public void Add(int employeeId, string text, bool isImportant)
        {
            var note = new Note(employeeId, text, isImportant);

            Add(note);
        }

        public void Add(Note note)
        {
            _notes.Add(note);
        }

        public void Remove(Note note)
        {
            _notes.Remove(note);
        }

        public void RemoveWhere(Predicate<Note> predicate)
        {
            _notes.RemoveWhere(predicate);
        }

        public byte[] ToBytes()
        {
            byte[] bytes = null;

            var formatter = new BinaryFormatter();

            using (var memoryStream = new MemoryStream()) {
                formatter.Serialize(memoryStream, this);
                bytes = memoryStream.ToArray();
            }

            return bytes;
        }

        public static NoteSet FromBytes(byte[] array)
        {
            NoteSet notes = null;

            var formatter = new BinaryFormatter();

            using (var memoryStream = new MemoryStream(array)) {
                notes = formatter.Deserialize(memoryStream) as NoteSet;
            }

            return notes;
        }
    }
}