﻿namespace nGen.Domain.Data.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}