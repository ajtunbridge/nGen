﻿namespace nGen.Domain.Data.Model
{
    public interface IPart : IEntity
    {
        int PartId { get; set; }
        string DrawingNumber { get; set; }
        string Name { get; set; }
        int CustomerId { get; set; }
    }
}