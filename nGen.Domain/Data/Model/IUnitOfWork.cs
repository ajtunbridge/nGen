﻿#region Using directives

using System;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Domain.Data.Model
{
    /// <summary>
    ///     Used for interfacing with data store
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        ///     Provides access to client settings
        /// </summary>
        IClientSettingRepository ClientSettings { get; }

        /// <summary>
        ///     Provides access to customer records
        /// </summary>
        ICustomerRepository Customers { get; }

        /// <summary>
        ///     Provides access to document records
        /// </summary>
        IDocumentRepository Documents { get; }

        /// <summary>
        ///     Provides access to document folder records
        /// </summary>
        IDocumentFolderRepository DocumentFolders { get; }

        /// <summary>
        ///     Provides access to document version records
        /// </summary>
        IDocumentVersionRepository DocumentVersions { get; }

        /// <summary>
        ///     Provides access to part records
        /// </summary>
        IPartRepository Parts { get; }

        /// <summary>
        ///     Provides access to part version records
        /// </summary>
        IPartVersionRepository PartVersions { get; }

        /// <summary>
        ///     Provides access to person records
        /// </summary>
        IPersonRepository People { get; }

        /// <summary>
        ///     Commit all changes to the database
        /// </summary>
        void Commit();

        /// <summary>
        ///     Discard any changes that have been made so far
        /// </summary>
        void DiscardChanges();
    }
}