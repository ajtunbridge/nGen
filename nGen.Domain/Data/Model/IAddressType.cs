﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nGen.Domain.Data.Model
{
    public interface IAddressType : IEntity
    {
        int AddressTypeId { get; set; }

        string Name { get; set; }
    }
}
