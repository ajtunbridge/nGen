﻿namespace nGen.Domain.Data.Model
{
    public interface IEmployee : IEntity
    {
        int EmployeeId { get; set; }
        int PersonId { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string Salt { get; set; }
        int EmployeeGroupId { get; set; }
    }
}