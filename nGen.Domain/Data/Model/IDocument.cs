﻿namespace nGen.Domain.Data.Model
{
    public interface IDocument : IEntity
    {
        int DocumentId { get; set; }

        string FileName { get; set; }

        bool IsEncrypted { get; set; }

        byte[] Permissions { get; set; }

        int? CustomerId { get; set; }

        int? DocumentFolderId { get; set; }

        int? PartId { get; set; }

        int? PartVersionId { get; set; }

        int DocumentTypeId { get; set; }
    }
}