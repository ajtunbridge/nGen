﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nGen.Domain.Data.Model
{
    public interface IAddress : IEntity
    {
        int AddressId { get; set; }

        string Line1 { get; set; }

        string Line2 { get; set; }

        string Line3 { get; set; }

        string City { get; set; }

        string Country { get; set; }

        string Postcode { get; set; }

        int AddressTypeId { get; set; }
    }
}
