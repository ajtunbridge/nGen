namespace nGen.Domain.Data.Model
{
    public interface IAssemblyVersionPart : IEntity
    {
        int AssemblyVersionPartId { get; set; }
        int Sequence { get; set; }
        int QtyRequired { get; set; }
        byte[] NotesBytes { get; set; }
        int AssemblyVersionId { get; set; }
        int PartVersionId { get; set; }
    }
}