﻿namespace nGen.Domain.Data.Model
{
    public interface IEmployeeGroup : IEntity
    {
        int EmployeeGroupId { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        byte[] Permissions { get; set; }
    }
}