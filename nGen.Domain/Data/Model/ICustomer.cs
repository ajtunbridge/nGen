﻿namespace nGen.Domain.Data.Model
{
    /// <summary>
    ///     Representation of a customer record
    /// </summary>
    public interface ICustomer : IEntity
    {
        /// <summary>
        ///     The primary key value of this customer
        /// </summary>
        int CustomerId { get; set; }

        /// <summary>
        ///     The name of this customer
        /// </summary>
        string Name { get; set; }

        /// <summary>
        ///     The payment terms for this customer
        /// </summary>
        string Terms { get; set; }

        /// <summary>
        ///     The VAT number of this customer
        /// </summary>
        string VATNumber { get; set; }

        /// <summary>
        ///     Indicator for whether this customer is approved to work for
        /// </summary>
        bool IsApproved { get; set; }

        /// <summary>
        ///     The tax code for this customer
        /// </summary>
        string TaxCode { get; set; }
    }
}