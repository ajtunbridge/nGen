﻿namespace nGen.Domain.Data.Model
{
    public interface IPartVersion : IEntity
    {
        int PartVersionId { get; set; }

        string VersionNumber { get; set; }

        byte[] PhotoBytes { get; set; }

        int PartId { get; set; }
    }
}