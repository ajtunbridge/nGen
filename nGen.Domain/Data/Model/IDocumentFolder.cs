﻿namespace nGen.Domain.Data.Model
{
    public interface IDocumentFolder : IEntity
    {
        int DocumentFolderId { get; set; }

        string Name { get; set; }

        int? ParentFolderId { get; set; }

        int? CustomerId { get; set; }

        int? PartId { get; set; }

        int? PartVersionId { get; set; }
    }
}