﻿#region Using directives

using System;

#endregion

namespace nGen.Domain.Data.Model
{
    /// <summary>
    ///     An entity representing a data object
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        ///     The date and time this record was first created
        /// </summary>
        DateTime CreatedOn { get; set; }

        /// <summary>
        ///     The date and time this record was last modified
        /// </summary>
        DateTime ModifiedOn { get; set; }

        /// <summary>
        ///     Primary key value of the employee who created this record
        /// </summary>
        int CreatedBy { get; set; }

        /// <summary>
        ///     Primary key value of the employee who modified this record
        /// </summary>
        int ModifiedBy { get; set; }
    }
}