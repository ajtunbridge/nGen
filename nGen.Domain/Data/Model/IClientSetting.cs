﻿namespace nGen.Domain.Data.Model
{
    public interface IClientSetting : IEntity
    {
        int ClientSettingId { get; set; }

        string WindowsDataDir { get; set; }

        string EncryptionPassword { get; set; }

        byte MaxDocumentVersionCount { get; set; }
    }
}