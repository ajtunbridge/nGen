﻿namespace nGen.Domain.Data.Model
{
    public interface IDocumentType : IEntity
    {
        int DocumentTypeId { get; set; }

        string Name { get; set; }

        string Description { get; set; }
    }
}