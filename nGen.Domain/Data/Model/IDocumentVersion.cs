﻿#region Using directives



#endregion

namespace nGen.Domain.Data.Model
{
    public interface IDocumentVersion : IEntity
    {
        int DocumentVersionId { get; set; }

        string MD5Hash { get; set; }

        int DocumentId { get; set; }
    }
}