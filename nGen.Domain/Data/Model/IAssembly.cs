﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nGen.Domain.Data.Model
{
    public interface IAssembly : IEntity
    {
        int AssemblyId { get; set; }
        string DrawingNumber { get; set; }
        string Name { get; set; }
        byte[] NotesBytes { get; set; }
        int CustomerId { get; set; }
    }
}
