﻿#region Using directives

using System;

#endregion

namespace nGen.Domain.Data.Model
{
    public interface IPerson : IEntity
    {
        int PersonId { get; set; }
        string Title { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        string Suffix { get; set; }
        string NickName { get; set; }
        DateTime? DateOfBirth { get; set; }
    }
}