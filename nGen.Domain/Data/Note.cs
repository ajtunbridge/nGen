﻿#region Using directives

using System;

#endregion

namespace nGen.Domain.Data
{
    [Serializable]
    public class Note : IEquatable<Note>
    {
        private readonly DateTime _createdOn;
        private readonly int _employeeId;
        private readonly bool _isImportant;
        private readonly string _text;

        public Note()
        {
        }

        public Note(int employeeId, string text, bool isImportant)
        {
            _employeeId = employeeId;
            _text = text;
            _isImportant = isImportant;
            _createdOn = DateTime.Now;
        }

        /// <summary>
        /// The date and time this note was created on
        /// </summary>
        public DateTime CreatedOn
        {
            get { return _createdOn; }
        }

        /// <summary>
        /// The ID of the employee who created this note
        /// </summary>
        public int EmployeeId
        {
            get { return _employeeId; }
        }

        /// <summary>
        /// The note's text
        /// </summary>
        public string Text
        {
            get { return _text; }
        }

        /// <summary>
        /// Flag to indicate whether this not is important or not
        /// </summary>
        public bool IsImportant
        {
            get { return _isImportant; }
        }

        #region IEquatable<Note> Members

        public bool Equals(Note other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return _employeeId == other._employeeId && _createdOn.Equals(other._createdOn);
        }

        #endregion

        public override int GetHashCode()
        {
            unchecked {
                return (_employeeId*397) ^ _createdOn.GetHashCode();
            }
        }

        public static bool operator ==(Note left, Note right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Note left, Note right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((Note) obj);
        }
    }
}