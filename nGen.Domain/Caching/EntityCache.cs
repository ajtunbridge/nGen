﻿#region Using directives

using System;
using System.Runtime.Caching;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Domain.Caching
{
    public sealed class EntityCache
    {
        private readonly MemoryCache _cache;
        private readonly CacheItemPolicy _defaultItemPolicy;

        public EntityCache()
        {
            _cache = new MemoryCache("EntityCache");

            // cache entities for 15 seconds after they were last accessed
            _defaultItemPolicy = new CacheItemPolicy {
                SlidingExpiration = new TimeSpan(0, 0, 15)
            };
        }

        public void Add<T>(string key, T entity) where T : class, IEntity
        {
            _cache.Add(key, entity, _defaultItemPolicy);
        }

        public T Get<T>(string key) where T : class, IEntity
        {
            return _cache.Get(key) as T;
        }
    }
}