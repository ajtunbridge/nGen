﻿#region Using directives

using System.IO;
using System.Threading.Tasks;

#endregion

namespace nGen.Domain.IO
{
    public class FileCopier
    {
        public event DocumentTransferCallback TransferProgress;

        private CancellableCallbackAction OnTransferProgress(string filename, string destinationdir, double progress)
        {
            var handler = TransferProgress;
            if (handler != null) {
                var result = handler(filename, destinationdir, progress);
                return result;
            }
            return CancellableCallbackAction.Continue;
        }

        public async Task CopyFileAsync(string sourceFile, string destinationFile)
        {
            await Task.Run(() => {
                var cancelCopy = false;

                using (var sourceStream = new FileStream(sourceFile, FileMode.Open, FileAccess.Read)) {
                    using (var destinationStream = new FileStream(destinationFile, FileMode.Create, FileAccess.Write)) {
                        var buffer = new byte[512];

                        var fileLength = sourceStream.Length;
                        long totalBytes = 0;
                        var currentBlockSize = 0;

                        var destinationDir = Path.GetDirectoryName(destinationFile);

                        while ((currentBlockSize = sourceStream.Read(buffer, 0, buffer.Length)) > 0) {
                            totalBytes += currentBlockSize;
                            var percentage = totalBytes*100.0/fileLength;

                            destinationStream.Write(buffer, 0, currentBlockSize);

                            var action = OnTransferProgress(sourceFile, destinationDir, percentage);

                            if (action == CancellableCallbackAction.Cancel) {
                                cancelCopy = true;
                                break;
                            }
                        }
                    }
                }

                if (cancelCopy && File.Exists(destinationFile)) {
                    File.Delete(destinationFile);
                }
            });
        }
    }
}