#region Using directives

using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

#endregion

namespace nGen.Domain.IO
{
    /// <summary>
    ///     Provides methods for serializing and deserializing objects
    /// </summary>
    public static class Serialization<T>
    {
        /// <summary>
        ///     Serializes the supplied object into a byte array
        /// </summary>
        /// <param name="obj">The object to serialize</param>
        /// <returns>The object serialized into a byte array</returns>
        public static byte[] Serialize(T obj)
        {
            MemoryStream stream = null;
            byte[] serializedData = null;

            try {
                var formatter = new BinaryFormatter();

                stream = new MemoryStream();

                formatter.Serialize(stream, obj);

                // Convert the streams' data into a byte array
                serializedData = stream.ToArray();
            }
            catch (Exception ex) {
                throw (ex);
            }
            finally {
                // Ensure we close the stream when we are finished with it
                if (stream != null) {
                    stream.Dispose();
                }
            }

            return serializedData;
        }

        /// <summary>
        ///     Deserializes the supplied byte array into an object
        /// </summary>
        /// <param name="data">The array of bytes containing the serialized object</param>
        /// <returns>An object of type T, deserialized from the byte array</returns>
        public static T Deserialize(byte[] data)
        {
            T deserializedObject;
            var formatter = new BinaryFormatter();
            MemoryStream stream = null;

            try {
                stream = new MemoryStream(data);
                stream.Position = 0;

                deserializedObject = (T) formatter.Deserialize(stream);
            }
            catch (Exception ex) {
                throw (ex);
            }
            finally {
                // Ensure we close the stream when we are finished with it
                if (stream != null) {
                    stream.Dispose();
                }
            }

            return deserializedObject;
        }
    }
}