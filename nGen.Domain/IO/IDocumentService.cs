﻿#region Using directives

using System.Threading.Tasks;
using nGen.Domain.Data.Model;
using nGen.Domain.Security;

#endregion

namespace nGen.Domain.IO
{
    public interface IDocumentService
    {
        event DocumentTransferCallback DocumentTransferProgressChanged;

        event EncryptionProgressCallback EncryptionProgressChanged;

        Task UploadAsync(string fileToUpload, IDocumentType documentType, DocumentPermissions permissions, ICustomer customer);

        Task UploadAsync(string fileToUpload, IDocumentType documentType, DocumentPermissions permissions, IDocumentFolder documentFolder);

        Task UploadAsync(string fileToUpload, IDocumentType documentType, DocumentPermissions permissions, IPart part);

        Task UploadAsync(string fileToUpload, IDocumentType documentType, DocumentPermissions permissions, IPartVersion partVersion);

        Task EncryptAsync(IDocument document);

        Task DecryptAsync(IDocument document);

        Task OpenReadOnlyAsync(IDocument document);

        Task CheckOutAsync(IDocument document);

        Task CheckInAsync(IDocument document);

        Task DeleteAsync(IDocument document);

        Task DeleteAsync(IDocumentVersion documentVersion);

        Task RenameAsync(IDocument document, string newName);
    }
}