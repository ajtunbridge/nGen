﻿namespace nGen.Domain
{
    public delegate CancellableCallbackAction EncryptionProgressCallback(string fileName, double progress);
}