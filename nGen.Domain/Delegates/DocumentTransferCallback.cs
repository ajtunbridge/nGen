﻿namespace nGen.Domain
{
    public delegate CancellableCallbackAction DocumentTransferCallback(string fileName, string status, double progress);
}