﻿namespace nGen.Domain
{
    /// <summary>
    ///     Actions that are allowed for cancellable events
    /// </summary>
    public enum CancellableCallbackAction
    {
        /// <summary>
        ///     Allow the process to continue
        /// </summary>
        Continue,

        /// <summary>
        ///     Cancel the process
        /// </summary>
        Cancel
    }
}