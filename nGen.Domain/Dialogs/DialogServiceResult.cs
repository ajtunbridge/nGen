﻿namespace nGen.Domain.Dialogs
{
    public enum DialogServiceResult
    {
        Yes,
        No,
        Cancel
    }
}