﻿namespace nGen.Domain.Dialogs
{
    public interface IDialogService
    {
        DialogServiceResult AskQuestion(string question);

        DialogServiceResult AskQuestion(string question, bool canCancel);

        void Notify(string message);

        void ShowError(string errorMessage);
    }
}