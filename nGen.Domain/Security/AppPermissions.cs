﻿#region Using directives

using System;
using nGen.Domain.IO;
using Security;

#endregion

namespace nGen.Domain.Security
{
    [Flags]
    public enum AppPermission
    {
        /// <summary>
        ///     Has full control of the system
        /// </summary>
        IsAdmin = 1,

        /// <summary>
        ///     Can create, edit and delete customer records
        /// </summary>
        CanManageCustomers = 1 << 1,

        /// <summary>
        ///     Can create, edit and delete parts
        /// </summary>
        CanManageParts = 1 << 2,

        /// <summary>
        ///     Can create, edit and delete employees
        /// </summary>
        CanManageEmployees = 1 << 3,

        /// <summary>
        ///     Can create, edit and delete documents
        /// </summary>
        CanManageDocuments = 1 << 4,

        /// <summary>
        ///     Can encrypt and decrypt documents
        /// </summary>
        CanEncryptDocuments = 1 << 5,

        /// <summary>
        ///     Default setting
        /// </summary>
        None = 1 << 6
    }

    [Serializable]
    public sealed class AppPermissions
    {
        private AppPermission _permissions = AppPermission.None;

        /// <summary>
        ///     The ID value of the employee group these permissions relate to. This will prevent the
        ///     copying of permissions from one record to another. Will need to be set after the employee
        ///     group record has been inserted into the database and therefore has an ID value to use!
        /// </summary>
        public int EmployeeGroupId { get; set; }

        public bool IsAdmin
        {
            get { return _permissions.Has(AppPermission.IsAdmin); }
        }

        public void GrantPermission(AppPermission permission)
        {
            _permissions.Add(permission);
        }

        public void RevokePermission(AppPermission permission)
        {
            _permissions.Remove(permission);
        }

        public bool HasPermission(AppPermission permission)
        {
            return _permissions.Has(permission);
        }

        public byte[] SerializeAndEncrypt()
        {
            var bytes = Serialization<AppPermissions>.Serialize(this);

            var encryptor = new SymmetricCryptoProvider(SymmetricCryptoAlgorithm.TripleDES);

            return encryptor.EncryptBuffer(bytes);
        }

        public static AppPermissions DecryptAndDeserialize(byte[] serialized)
        {
            var decryptor = new SymmetricCryptoProvider(SymmetricCryptoAlgorithm.TripleDES);

            var decryptedBytes = decryptor.DecryptBuffer(serialized);

            return Serialization<AppPermissions>.Deserialize(decryptedBytes);
        }
    }
}