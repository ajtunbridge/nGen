﻿#region Using directives

using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

#endregion

namespace nGen.Domain.Security
{
    public class PBKDF2PasswordService : IPasswordService
    {
        private const byte SaltSize = 16;
        private const int Iterations = 10000;

        #region IPasswordService Members

        public EncryptedPassword EncryptPassword(string plainPassword)
        {
            var gch = GCHandle.Alloc(plainPassword, GCHandleType.Pinned);

            var hashed = new EncryptedPassword();

            var salt = GenerateSalt();

            hashed.Hash = ComputeHash(plainPassword, salt);
            hashed.Salt = Convert.ToBase64String(salt);

            ZeroMemory(gch.AddrOfPinnedObject(), plainPassword.Length*2);

            gch.Free();

            return hashed;
        }

        public bool AreEqual(string plainPassword, string hash, string salt)
        {
            var gch = GCHandle.Alloc(plainPassword, GCHandleType.Pinned);

            var saltBytes = Convert.FromBase64String(salt);

            var newHash = ComputeHash(plainPassword, saltBytes);

            ZeroMemory(gch.AddrOfPinnedObject(), plainPassword.Length*2);

            gch.Free();

            return newHash == hash;
        }

        #endregion

        //  Call this function to remove the plaintext password from memory after use
        [DllImport("KERNEL32.DLL", EntryPoint = "RtlZeroMemory")]
        public static extern bool ZeroMemory(IntPtr Destination, int Length);

        private string ComputeHash(string text, byte[] salt)
        {
            using (var pbkdf2 = new Rfc2898DeriveBytes(text, salt, Iterations)) {
                var key = pbkdf2.GetBytes(64);
                return Convert.ToBase64String(key);
            }
        }

        private byte[] GenerateSalt()
        {
            using (var rand = RandomNumberGenerator.Create()) {
                var saltBytes = new byte[SaltSize];

                rand.GetBytes(saltBytes);

                return saltBytes;
            }
        }
    }
}