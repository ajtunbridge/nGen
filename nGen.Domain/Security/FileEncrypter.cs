﻿#region Using directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;

#endregion

namespace nGen.Domain.Security
{
    /// <summary>
    ///     Provides methods for the encryption and decryption of files using the AES algorithm and a 128 bit key
    /// </summary>
    public sealed class FileEncrypter
    {
        #region Constants

        private const int KeySize = 128;
        private const int BlockSize = 128;
        private const int Iterations = 5000;
        private const int SaltSize = 16;

        #endregion

        #region Events

        /// <summary>
        ///     This event is fired when the encryption progresses. The <see cref="EncryptionProgressCallback" /> callback allows
        ///     for cancellation
        /// </summary>
        public event EncryptionProgressCallback EncryptionProgressChanged;

        #endregion

        #region Public methods

        /// <summary>
        ///     Encrypts the specified file, in place, asynchronously using the AES symmetric algorithm and the supplied password
        /// </summary>
        /// <param name="source">The file to encrypt</param>
        /// <param name="password">The password to encrypt the file with</param>
        /// <returns></returns>
        public async Task EncryptFileAsync(string source, string password)
        {
            if (!File.Exists(source)) {
                throw new FileNotFoundException("Source file does not exist. Encryption cancelled.");
            }

            string directory = Path.GetDirectoryName(source);
            string tempFileName = Path.GetRandomFileName();
            string destinationFile = Path.Combine(directory, tempFileName);

            await EncryptFileAsync(source, destinationFile, password, true);

            File.Move(destinationFile, source);
        }

        /// <summary>
        ///     Encrypts the supplied source file to the destination file asynchronously using the AES symmetric algorithm and the
        ///     supplied password.
        /// </summary>
        /// <param name="source">The file to encrypt</param>
        /// <param name="destination">The file to save the encrypted data to</param>
        /// <param name="password">The password to encrypt the file with</param>
        /// <param name="deleteSource">Whether to delete the source file after encrypting</param>
        /// <returns></returns>
        public async Task EncryptFileAsync(string source, string destination, string password, bool deleteSource)
        {
            if (!File.Exists(source)) {
                throw new FileNotFoundException("Source file does not exist. Encryption cancelled.");
            }

            await Task.Run(() => {
                bool encryptionCancelled = false;

                byte[] passwordBytes = StringToBytes(password);
                byte[] saltBytes = GetRandomBytes(SaltSize);

                using (var aes = new AesManaged()) {
                    aes.KeySize = KeySize;
                    aes.BlockSize = BlockSize;
                    aes.Mode = CipherMode.CBC;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, Iterations);

                    aes.Key = key.GetBytes(aes.KeySize/8);
                    aes.IV = key.GetBytes(aes.BlockSize/8);

                    ICryptoTransform transform = aes.CreateEncryptor(aes.Key, aes.IV);

                    using (
                        var destStream = new FileStream(destination, FileMode.Create, FileAccess.Write, FileShare.Write)
                        ) {
                        File.SetAttributes(destination, FileAttributes.Hidden);

                        using (var cryptoStream = new CryptoStream(destStream, transform, CryptoStreamMode.Write)) {
                            using (var sourceStream = new FileStream(source, FileMode.Open, FileAccess.Read)) {
                                var buffer = new byte[1024*1024]; // 1MB buffer

                                long fileLength = sourceStream.Length;
                                long totalBytes = 0;
                                int currentBlockSize;

                                while ((currentBlockSize = sourceStream.Read(buffer, 0, buffer.Length)) > 0) {
                                    totalBytes += currentBlockSize;
                                    double percentage = totalBytes*100.0/fileLength;

                                    cryptoStream.Write(buffer, 0, currentBlockSize);

                                    CancellableCallbackAction action = OnEncryptionProgressChanged(source, percentage);

                                    if (action == CancellableCallbackAction.Cancel) {
                                        encryptionCancelled = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    // append the salt to the end of the file
                    using (var stream = new FileStream(destination, FileMode.Append)) {
                        stream.Write(saltBytes, 0, saltBytes.Length);
                    }

                    // unhide the file when it is finished with
                    File.SetAttributes(destination, FileAttributes.Normal);
                }

                if (deleteSource || encryptionCancelled && File.Exists(destination)) {
                    File.Delete(source);
                }
            });
        }

        /// <summary>
        ///     Decrypts the specified file, in place, asynchronously using the AES symmetric algorithm and the supplied password
        /// </summary>
        /// <param name="source">The file to decrypt</param>
        /// <param name="password">The password to decrypt the file with</param>
        /// <returns></returns>
        public async Task DecryptFileAsync(string source, string password)
        {
            if (!File.Exists(source)) {
                throw new FileNotFoundException("Source file does not exist. Decryption cancelled.");
            }

            string directory = Path.GetDirectoryName(source);
            string tempFileName = Path.GetRandomFileName();
            string destinationFile = Path.Combine(directory, tempFileName);

            await DecryptFileAsync(source, destinationFile, password, true);

            File.Move(destinationFile, source);
        }

        /// <summary>
        ///     Decrypts the supplied source file asynchronously using the AES symmetric algorithm and the supplied password
        /// </summary>
        /// <param name="source">The file to decrypt</param>
        /// <param name="destination">The file path to save the decrypted file to</param>
        /// <param name="password">The password used to encrypt the file with</param>
        /// <param name="deleteSource">Whether to delete the source file after decrypting</param>
        /// <returns></returns>
        public async Task DecryptFileAsync(string source, string destination, string password, bool deleteSource)
        {
            if (!File.Exists(source)) {
                throw new FileNotFoundException("Source file does not exist. Decryption cancelled.");
            }

            await Task.Run(() => {
                var saltBytes = new byte[SaltSize];

                using (var sourceStream = new FileStream(source, FileMode.Open, FileAccess.ReadWrite)) {
                    // retrieve salt bytes appended to the end of the file
                    using (var reader = new BinaryReader(sourceStream)) {
                        long startOfSalt = sourceStream.Length - SaltSize;

                        reader.BaseStream.Seek(startOfSalt, SeekOrigin.Begin);
                        reader.Read(saltBytes, 0, SaltSize);
                    }
                }

                // remove the salt bytes from the file
                using (var sourceStream = new FileStream(source, FileMode.Open, FileAccess.Write)) {
                    sourceStream.SetLength(sourceStream.Length - SaltSize);
                }

                try {
                    bool encryptionCancelled = false;

                    using (var aes = new AesManaged()) {
                        aes.KeySize = KeySize;
                        aes.BlockSize = BlockSize;
                        aes.Mode = CipherMode.CBC;

                        byte[] passwordBytes = StringToBytes(password);

                        var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, Iterations);

                        aes.Key = key.GetBytes(aes.KeySize/8);
                        aes.IV = key.GetBytes(aes.BlockSize/8);

                        ICryptoTransform transform = aes.CreateDecryptor(aes.Key, aes.IV);

                        using (
                            var destStream = new FileStream(destination, FileMode.Create, FileAccess.Write,
                                FileShare.Write)) {
                            // we don't want to see the temp file really
                            File.SetAttributes(destination, FileAttributes.Hidden);

                            using (var cryptoStream = new CryptoStream(destStream, transform, CryptoStreamMode.Write)) {
                                using (
                                    var sourceStream = new FileStream(source, FileMode.Open, FileAccess.Read,
                                        FileShare.Read)) {
                                    var buffer = new byte[1024*1024]; // 1MB buffer

                                    long fileLength = sourceStream.Length;
                                    long totalBytes = 0;
                                    int currentBlockSize = 0;

                                    while ((currentBlockSize = sourceStream.Read(buffer, 0, buffer.Length)) > 0) {
                                        totalBytes += currentBlockSize;
                                        double percentage = totalBytes*100.0/fileLength;

                                        cryptoStream.Write(buffer, 0, currentBlockSize);

                                        CancellableCallbackAction action = OnEncryptionProgressChanged(source, percentage);

                                        if (action == CancellableCallbackAction.Cancel) {
                                            encryptionCancelled = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        // unhide the file when we're done
                        File.SetAttributes(destination, FileAttributes.Normal);
                    }

                    if (deleteSource || encryptionCancelled && File.Exists(destination)) {
                        File.Delete(source);
                    }
                }
                finally {
                    if (!deleteSource) {
                        // we must append the salt bytes to the source file when done otherwise
                        // we will never be able to decrypt the file again
                        using (var stream = new FileStream(source, FileMode.Append, FileAccess.Write)) {
                            stream.Write(saltBytes, 0, saltBytes.Length);
                        }
                    }
                }
            });
        }

        #endregion

        #region Private methods

        /// <summary>
        ///     Converts a string directly into a byte array without using encoding
        /// </summary>
        /// <param name="value">The string to convert into a byte array</param>
        /// <returns></returns>
        private byte[] StringToBytes(string value)
        {
            var bytes = new byte[value.Length*sizeof (char)];
            Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        /// <summary>
        ///     Generate a random byte array of the given length
        /// </summary>
        /// <param name="length">The length of the array to create</param>
        /// <returns></returns>
        private byte[] GetRandomBytes(int length)
        {
            var bytes = new byte[length];
            RandomNumberGenerator.Create().GetBytes(bytes);
            return bytes;
        }

        private CancellableCallbackAction OnEncryptionProgressChanged(string fileName,  double progress)
        {
            EncryptionProgressCallback handler = EncryptionProgressChanged;

            if (handler != null) {
                CancellableCallbackAction result = handler(fileName, progress);
                return result;
            }

            return CancellableCallbackAction.Continue;
        }

        #endregion
    }
}