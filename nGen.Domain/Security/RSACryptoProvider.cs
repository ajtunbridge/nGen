#region Using directives

using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;

#endregion

namespace nGen.Domain.Security
{
    /// <summary>
    ///     Provides methods for public/private key encryption of strings
    /// </summary>
    public sealed class RSACryptoProvider : IDisposable
    {
        #region Constructors

        /// <summary>
        ///     Constructs using the default key size of 1024 bits
        /// </summary>
        public RSACryptoProvider()
            : this(1024)
        {
            _rsa.PersistKeyInCsp = false;
        }

        /// <summary>
        ///     Constructs using the key size specified
        /// </summary>
        /// <param name="keySize">The size of the key in bits (1024 or 2048)</param>
        public RSACryptoProvider(int keySize)
        {
            if (keySize == 1024 || keySize == 2048) {
                _rsa = new RSACryptoServiceProvider(keySize);
            }
            else {
                throw new ArgumentException("Set the key size to either 1024 or 2048 bit");
            }
        }

        #endregion

        #region Private Variables

        /// <summary>
        ///     The RSA provider to use for cryptographic functions
        /// </summary>
        private RSACryptoServiceProvider _rsa;

        #endregion

        #region Properties

        /// <summary>
        ///     This key can be distributed publicly for encrypting data
        /// </summary>
        public Byte[] PublicKey
        {
            get { return _rsa.ExportCspBlob(false); }
        }

        /// <summary>
        ///     This key MUST be kept private (ideally on a removable drive). This
        ///     is the key used for decrypting data that has been encrypted using
        ///     the public key
        /// </summary>
        public Byte[] PrivateKey
        {
            get { return _rsa.ExportCspBlob(true); }
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Encrypts the string supplied using the public key provided
        /// </summary>
        /// <param name="plainText">The text to encrypt</param>
        /// <param name="publicKey">The public key to use for encryption</param>
        /// <returns></returns>
        public String EncryptString(String plainText, Byte[] publicKey)
        {
            var cipherText = new StringBuilder();

            _rsa = new RSACryptoServiceProvider();
            _rsa.ImportCspBlob(publicKey);

            var charArray = plainText.ToCharArray();

            var numberOfBlocks = (plainText.Length/32) + 1;

            var byteBlockArray = new Byte[numberOfBlocks][];
            var incrementer = 0;
            for (var i = 1; i <= numberOfBlocks; i++) {
                if (i == numberOfBlocks) {
                    byteBlockArray[i - 1] = Encoding.ASCII.GetBytes(
                        charArray, incrementer, charArray.Length - incrementer);
                }
                else {
                    byteBlockArray[i - 1] = Encoding.ASCII.GetBytes(charArray, incrementer, 32);
                    incrementer += 32;
                }
            }

            for (var j = 0; j < byteBlockArray.Length; j++) {
                cipherText.Append(Convert.ToBase64String(_rsa.Encrypt(byteBlockArray[j], true)));
            }

            return cipherText.ToString();
        }

        /// <summary>
        ///     Decrypts the supplied cipher text using the specified private key
        /// </summary>
        /// <param name="cipherText">The cipher text to decrypt</param>
        /// <param name="privateKey">The private key to use for decryption</param>
        /// <returns></returns>
        public String DecryptString(String cipherText, Byte[] privateKey)
        {
            if (cipherText.IndexOf("=") == -1) {
                throw new ArgumentException("The string supplied is not in a valid format",
                    "cipherText");
            }

            var plainText = new StringBuilder();

            try {
                var rsa = new RSACryptoServiceProvider();
                rsa.ImportCspBlob(privateKey);

                var encryptedBlock = String.Empty;

                var encryptedBlocks = new Queue();
                while (cipherText.Length != 0) {
                    if (rsa.KeySize == 1024) {
                        encryptedBlock = cipherText.Substring(0, cipherText.IndexOf("=") + 1);
                        encryptedBlocks.Enqueue(encryptedBlock);
                        cipherText = cipherText.Remove(0, encryptedBlock.Length);
                    }
                    else {
                        encryptedBlock = cipherText.Substring(0, cipherText.IndexOf("==") + 2);
                        encryptedBlocks.Enqueue(encryptedBlock);
                        cipherText = cipherText.Remove(0, encryptedBlock.Length);
                    }
                }

                encryptedBlocks.TrimToSize();
                var numberOfBlocks = encryptedBlocks.Count;
                for (var i = 1; i <= numberOfBlocks; i++) {
                    encryptedBlock = (String) encryptedBlocks.Dequeue();

                    plainText.Append(Encoding.ASCII.GetString(
                        rsa.Decrypt(Convert.FromBase64String(encryptedBlock), true)));
                }
            }
            catch (Exception ex) {
                throw (ex);
            }

            return plainText.ToString();
        }

        public string PrivateKeyToXML()
        {
            return _rsa.ToXmlString(true);
        }

        public string PublicKeyToXML()
        {
            return _rsa.ToXmlString(false);
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        ///     Releases resources used by the RSACryptoServiceProvider
        /// </summary>
        public void Dispose()
        {
            if (_rsa != null) {
                _rsa.Clear();
                _rsa = null;
            }
        }

        #endregion
    }
}