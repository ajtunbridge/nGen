﻿#region Using directives

using System;
using System.Collections.Generic;
using nGen.Domain.Data.Model;
using nGen.Domain.IO;
using Security;

#endregion

namespace nGen.Domain.Security
{
    [Serializable]
    public sealed class DocumentPermissions
    {
        private readonly Dictionary<int, DocumentAccess> _employeeAccessRights = new Dictionary<int, DocumentAccess>();

        private readonly Dictionary<int, DocumentAccess> _employeeGroupAccessRights =
            new Dictionary<int, DocumentAccess>();

        /// <summary>
        ///     The ID value of the document these permissions relate to. This will prevent the
        ///     copying of permissions from one record to another. Will need to be set after the document
        ///     record has been inserted into the database and therefore has an ID value to use!
        /// </summary>
        public int DocumentId { get; set; }

        /// <summary>
        ///     Grants the specified <see cref="IEmployee" /> the supplied <see cref="DocumentAccess" />
        /// </summary>
        /// <param name="employee">The <see cref="IEmployee" /> to grant the access to</param>
        /// <param name="access">The access to grant the <see cref="IEmployee" /></param>
        public void GrantAccess(IEmployee employee, DocumentAccess access)
        {
            if (_employeeAccessRights.ContainsKey(employee.EmployeeId)) {
                _employeeAccessRights[employee.EmployeeId] = access;
            }
            else {
                _employeeAccessRights.Add(employee.EmployeeId, access);
            }
        }

        /// <summary>
        ///     Grants the specified <see cref="IEmployeeGroup" /> the supplied <see cref="DocumentAccess" />
        /// </summary>
        /// <param name="group">The <see cref="IEmployeeGroup" /> to grant the access to</param>
        /// <param name="access">The access to grant the <see cref="IEmployeeGroup" /></param>
        public void GrantAccess(IEmployeeGroup group, DocumentAccess access)
        {
            if (_employeeGroupAccessRights.ContainsKey(group.EmployeeGroupId)) {
                _employeeGroupAccessRights[group.EmployeeGroupId] = access;
            }
            else {
                _employeeGroupAccessRights.Add(group.EmployeeGroupId, access);
            }
        }

        /// <summary>
        ///     Revokes access to the document for the specified <see cref="IEmployee" />
        /// </summary>
        /// <param name="employee">The <see cref="IEmployee" /> whose access to revoke</param>
        public void RevokeAccess(IEmployee employee)
        {
            if (_employeeAccessRights.ContainsKey(employee.EmployeeId)) {
                _employeeAccessRights.Remove(employee.EmployeeId);
            }
        }

        /// <summary>
        ///     Revokes access to the document for the specified <see cref="IEmployeeGroup" />
        /// </summary>
        /// <param name="group">The <see cref="IEmployeeGroup" /> whose access to revoke</param>
        public void RevokeAccess(IEmployeeGroup group)
        {
            if (_employeeGroupAccessRights.ContainsKey(group.EmployeeGroupId)) {
                _employeeGroupAccessRights.Remove(group.EmployeeGroupId);
            }
        }

        /// <summary>
        ///     Determine if the specified <see cref="IEmployee" /> has read access to the document. <see cref="IEmployeeGroup" />
        ///     level permissions
        ///     supersede <see cref="IEmployee" /> level permissions
        /// </summary>
        /// <param name="employee">The <see cref="IEmployee" /> to check for read access</param>
        /// <returns></returns>
        public bool CanRead(IEmployee employee)
        {
            // TODO: grant the admin account full access

            // if the employees group has permissions set, return those
            if (_employeeGroupAccessRights.ContainsKey(employee.EmployeeGroupId)) {
                return true;
            }

            // then check if the employee has permissions set
            if (_employeeAccessRights.ContainsKey(employee.EmployeeId)) {
                return true;
            }

            // otherwise, the employee has no read access
            return false;
        }

        /// <summary>
        ///     Determine if the specified <see cref="IEmployee" /> has write access to the document. <see cref="IEmployeeGroup" />
        ///     level permissions
        ///     supersede <see cref="IEmployee" /> level permissions
        /// </summary>
        /// <param name="employee">The <see cref="IEmployee" /> to check for write access</param>
        /// <returns></returns>
        public bool CanWrite(IEmployee employee)
        {
            // if the employees group has permissions set, return those
            if (_employeeGroupAccessRights.ContainsKey(employee.EmployeeGroupId)) {
                return _employeeGroupAccessRights[employee.EmployeeGroupId] == DocumentAccess.ReadWrite;
            }

            // then check if the employee has permissions set
            if (_employeeAccessRights.ContainsKey(employee.EmployeeId)) {
                return _employeeAccessRights[employee.EmployeeId] == DocumentAccess.ReadWrite;
            }

            // otherwise, the employee has no write access
            return false;
        }

        public byte[] SerializeAndEncrypt()
        {
            var bytes = Serialization<DocumentPermissions>.Serialize(this);

            var encryptor = new SymmetricCryptoProvider(SymmetricCryptoAlgorithm.TripleDES);

            return encryptor.EncryptBuffer(bytes);
        }

        public static DocumentPermissions DecryptAndDeserialize(byte[] serialized)
        {
            var decryptor = new SymmetricCryptoProvider(SymmetricCryptoAlgorithm.TripleDES);

            var decryptedBytes = decryptor.DecryptBuffer(serialized);

            return Serialization<DocumentPermissions>.Deserialize(decryptedBytes);
        }
    }

    public enum DocumentAccess
    {
        /// <summary>
        ///     The user can only read the document
        /// </summary>
        Read,

        /// <summary>
        ///     The user can read or write to the documnet
        /// </summary>
        ReadWrite
    }
}