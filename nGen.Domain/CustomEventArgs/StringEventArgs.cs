﻿using System;

namespace nGen.Domain
{
    public sealed class StringEventArgs : EventArgs
    {
        private readonly string _value;

        public string Value
        {
            get { return _value; }
        }

        public StringEventArgs(string value)
        {
            _value = value;
        }
    }
}
