USE [master]
GO
/****** Object:  Database [nGen_Production]    Script Date: 03/02/2015 19:50:33 ******/
CREATE DATABASE [nGen_Production]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'nGen_Production', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\nGen_Production.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'nGen_Production_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\nGen_Production_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [nGen_Production] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [nGen_Production].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [nGen_Production] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [nGen_Production] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [nGen_Production] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [nGen_Production] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [nGen_Production] SET ARITHABORT OFF 
GO
ALTER DATABASE [nGen_Production] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [nGen_Production] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [nGen_Production] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [nGen_Production] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [nGen_Production] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [nGen_Production] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [nGen_Production] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [nGen_Production] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [nGen_Production] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [nGen_Production] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [nGen_Production] SET  DISABLE_BROKER 
GO
ALTER DATABASE [nGen_Production] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [nGen_Production] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [nGen_Production] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [nGen_Production] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [nGen_Production] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [nGen_Production] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [nGen_Production] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [nGen_Production] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [nGen_Production] SET  MULTI_USER 
GO
ALTER DATABASE [nGen_Production] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [nGen_Production] SET DB_CHAINING OFF 
GO
ALTER DATABASE [nGen_Production] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [nGen_Production] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [nGen_Production]
GO
/****** Object:  Table [dbo].[Addresses]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Addresses](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[Line1] [varchar](100) NOT NULL,
	[Line2] [varchar](100) NULL,
	[Line3] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[County] [varchar](100) NULL,
	[Country] [varchar](100) NULL,
	[Postcode] [varchar](8) NULL,
	[AddressTypeId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddressTypes]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressTypes](
	[AddressTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_AddressTypes] PRIMARY KEY CLUSTERED 
(
	[AddressTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Assemblies]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Assemblies](
	[AssemblyId] [int] IDENTITY(1,1) NOT NULL,
	[DrawingNumber] [varchar](50) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[NotesBytes] [varbinary](max) NULL,
	[CustomerId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_PartAssemblies] PRIMARY KEY CLUSTERED 
(
	[AssemblyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssemblyVersionParts]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssemblyVersionParts](
	[AssemblyVersionPartId] [int] IDENTITY(1,1) NOT NULL,
	[Sequence] [int] NOT NULL,
	[QtyRequired] [int] NOT NULL,
	[NotesBytes] [varbinary](max) NULL,
	[AssemblyVersionId] [int] NOT NULL,
	[PartVersionId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_AssemblyVersionParts] PRIMARY KEY CLUSTERED 
(
	[AssemblyVersionPartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UIX_AssemblyVersionParts_AssemblyVersionId_PartVersionId] UNIQUE NONCLUSTERED 
(
	[AssemblyVersionId] ASC,
	[PartVersionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssemblyVersions]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssemblyVersions](
	[AssemblyVersionId] [int] IDENTITY(1,1) NOT NULL,
	[VersionNumber] [varchar](10) NOT NULL,
	[NotesBytes] [varbinary](max) NULL,
	[PhotoBytes] [varbinary](max) NULL,
	[AssemblyId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_AssemblyVersions] PRIMARY KEY CLUSTERED 
(
	[AssemblyVersionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClientSettings]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientSettings](
	[ClientSettingId] [int] IDENTITY(1,1) NOT NULL,
	[WindowsDataDir] [varchar](255) NOT NULL,
	[EncryptionPassword] [varchar](64) NOT NULL,
	[MaxDocumentVersionCount] [tinyint] NOT NULL CONSTRAINT [DF_ClientSettings_MaxDocumentVersionCount]  DEFAULT ((3)),
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_ClientSettings] PRIMARY KEY CLUSTERED 
(
	[ClientSettingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactDetails]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactDetails](
	[ContactDetailId] [int] IDENTITY(1,1) NOT NULL,
	[Value] [varchar](150) NOT NULL,
	[ContactDetailTypeId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_ContactInfo] PRIMARY KEY CLUSTERED 
(
	[ContactDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactDetailTypes]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactDetailTypes](
	[ContactDetailTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_ContactDetailTypes] PRIMARY KEY CLUSTERED 
(
	[ContactDetailTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerAddresses]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerAddresses](
	[CustomerAddressId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[AddressId] [int] NOT NULL,
 CONSTRAINT [PK_CustomerAddresses] PRIMARY KEY CLUSTERED 
(
	[CustomerAddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UIX_CustomerAddresses_CustomerId_AddressId] UNIQUE NONCLUSTERED 
(
	[CustomerId] ASC,
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerContactDetails]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerContactDetails](
	[CustomerContactDetailId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ContactDetailId] [int] NOT NULL,
 CONSTRAINT [PK_CustomerContactDetails] PRIMARY KEY CLUSTERED 
(
	[CustomerContactDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UIX_CustomerContactDetails_CustomerId_ContactDetailId] UNIQUE NONCLUSTERED 
(
	[CustomerId] ASC,
	[ContactDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerContacts]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerContacts](
	[CustomerContactId] [int] IDENTITY(1,1) NOT NULL,
	[Role] [varchar](50) NULL,
	[CustomerId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_CustomerContacts] PRIMARY KEY CLUSTERED 
(
	[CustomerContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Terms] [nvarchar](50) NULL,
	[VATNumber] [nvarchar](20) NULL,
	[IsApproved] [bit] NOT NULL,
	[TaxCode] [nvarchar](2) NULL,
	[NotesBytes] [varbinary](max) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentFolders]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentFolders](
	[DocumentFolderId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[ParentFolderId] [int] NULL,
	[CustomerId] [int] NULL,
	[PartId] [int] NULL,
	[PartVersionId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_DocumentFolders] PRIMARY KEY CLUSTERED 
(
	[DocumentFolderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Documents]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Documents](
	[DocumentId] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](100) NOT NULL,
	[IsEncrypted] [bit] NOT NULL CONSTRAINT [DF_Documents_IsEncrypted]  DEFAULT ((0)),
	[Permissions] [varbinary](max) NULL,
	[NotesBytes] [varbinary](max) NULL,
	[DocumentTypeId] [int] NOT NULL,
	[CustomerId] [int] NULL,
	[DocumentFolderId] [int] NULL,
	[PartId] [int] NULL,
	[PartVersionId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_Documents] PRIMARY KEY CLUSTERED 
(
	[DocumentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentTypes]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentTypes](
	[DocumentTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[Description] [varchar](255) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_DocumentTypes] PRIMARY KEY CLUSTERED 
(
	[DocumentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentVersions]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentVersions](
	[DocumentVersionId] [int] IDENTITY(1,1) NOT NULL,
	[MD5Hash] [char](32) NOT NULL,
	[Changes] [varchar](max) NULL,
	[DocumentId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_DocumentVersions] PRIMARY KEY CLUSTERED 
(
	[DocumentVersionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeGroups]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeGroups](
	[EmployeeGroupId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[Permissions] [varbinary](max) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_EmployeeGroups] PRIMARY KEY CLUSTERED 
(
	[EmployeeGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employees](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](30) NOT NULL,
	[Password] [char](88) NOT NULL,
	[Salt] [char](24) NOT NULL,
	[PersonId] [int] NOT NULL,
	[EmployeeGroupId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PartFixtures]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartFixtures](
	[PartFixtureId] [int] IDENTITY(1,1) NOT NULL,
	[Location] [varchar](30) NOT NULL,
	[Contents] [varchar](max) NULL,
	[PhotoBytes] [varbinary](max) NULL,
	[PartVersionId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_PartFixtures] PRIMARY KEY CLUSTERED 
(
	[PartFixtureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Parts]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Parts](
	[PartId] [int] IDENTITY(1,1) NOT NULL,
	[DrawingNumber] [varchar](50) NOT NULL,
	[Name] [varchar](100) NULL,
	[NotesBytes] [varbinary](max) NULL,
	[CustomerId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_Parts] PRIMARY KEY CLUSTERED 
(
	[PartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PartVersions]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartVersions](
	[PartVersionId] [int] IDENTITY(1,1) NOT NULL,
	[VersionNumber] [varchar](10) NOT NULL,
	[NotesBytes] [varbinary](max) NULL,
	[PhotoBytes] [varbinary](max) NULL,
	[PartId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_PartVersions] PRIMARY KEY CLUSTERED 
(
	[PartVersionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[People]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[People](
	[PersonId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[MiddleName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Suffix] [nvarchar](50) NULL,
	[NickName] [nvarchar](50) NULL,
	[DateOfBirth] [datetime] NULL,
	[NotesBytes] [varbinary](max) NULL,
	[PhotoBytes] [varbinary](max) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_People] PRIMARY KEY CLUSTERED 
(
	[PersonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonAddresses]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonAddresses](
	[PersonAddressId] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[AddressId] [int] NOT NULL,
 CONSTRAINT [PK_PersonAddresses] PRIMARY KEY CLUSTERED 
(
	[PersonAddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UIX_PersonAddresses_PersonId_AddressId] UNIQUE NONCLUSTERED 
(
	[PersonId] ASC,
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersonContactDetails]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonContactDetails](
	[PersonContactDetailId] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[ContactDetailId] [int] NOT NULL,
 CONSTRAINT [PK_PersonContactDetails] PRIMARY KEY CLUSTERED 
(
	[PersonContactDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UIX_PersonContactDetails_PersonId_ContactDetailId] UNIQUE NONCLUSTERED 
(
	[PersonId] ASC,
	[ContactDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuickMessages]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuickMessages](
	[QuickMessageId] [int] IDENTITY(1,1) NOT NULL,
	[Text] [varchar](max) NULL,
	[SentOn] [datetime] NULL,
	[HasBeenSeen] [bit] NOT NULL,
	[FromEmployeeId] [int] NOT NULL,
	[ToEmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_QuickMessages] PRIMARY KEY CLUSTERED 
(
	[QuickMessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierContacts]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SupplierContacts](
	[SupplierContactId] [int] IDENTITY(1,1) NOT NULL,
	[Role] [varchar](50) NULL,
	[NotesBytes] [varbinary](max) NULL,
	[SupplierId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_SupplierContacts] PRIMARY KEY CLUSTERED 
(
	[SupplierContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Suppliers]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Suppliers](
	[SupplierId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Terms] [varchar](50) NULL,
	[IsApproved] [bit] NOT NULL,
	[NotesBytes] [varbinary](max) NULL,
	[SupplierTypeId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_Suppliers] PRIMARY KEY CLUSTERED 
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierTypes]    Script Date: 03/02/2015 19:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SupplierTypes](
	[SupplierTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_SupplierTypes] PRIMARY KEY CLUSTERED 
(
	[SupplierTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_FK_Documents_DocumentTypes]    Script Date: 03/02/2015 19:50:33 ******/
CREATE NONCLUSTERED INDEX [IX_FK_Documents_DocumentTypes] ON [dbo].[Documents]
(
	[DocumentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_DocumentVersions_Documents]    Script Date: 03/02/2015 19:50:33 ******/
CREATE NONCLUSTERED INDEX [IX_FK_DocumentVersions_Documents] ON [dbo].[DocumentVersions]
(
	[DocumentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_Employees_EmployeeGroups]    Script Date: 03/02/2015 19:50:33 ******/
CREATE NONCLUSTERED INDEX [IX_FK_Employees_EmployeeGroups] ON [dbo].[Employees]
(
	[EmployeeGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_Employees_People]    Script Date: 03/02/2015 19:50:33 ******/
CREATE NONCLUSTERED INDEX [IX_FK_Employees_People] ON [dbo].[Employees]
(
	[PersonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_PartFixtures_Parts]    Script Date: 03/02/2015 19:50:33 ******/
CREATE NONCLUSTERED INDEX [IX_FK_PartFixtures_Parts] ON [dbo].[PartFixtures]
(
	[PartVersionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_Parts_Customers]    Script Date: 03/02/2015 19:50:33 ******/
CREATE NONCLUSTERED INDEX [IX_FK_Parts_Customers] ON [dbo].[Parts]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_PartVersions_Parts]    Script Date: 03/02/2015 19:50:33 ******/
CREATE NONCLUSTERED INDEX [IX_FK_PartVersions_Parts] ON [dbo].[PartVersions]
(
	[PartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QuickMessages] ADD  CONSTRAINT [DF_InternalMessage_HasBeenRead]  DEFAULT ((0)) FOR [HasBeenSeen]
GO
ALTER TABLE [dbo].[Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Addresses_AddressTypes] FOREIGN KEY([AddressTypeId])
REFERENCES [dbo].[AddressTypes] ([AddressTypeId])
GO
ALTER TABLE [dbo].[Addresses] CHECK CONSTRAINT [FK_Addresses_AddressTypes]
GO
ALTER TABLE [dbo].[Assemblies]  WITH CHECK ADD  CONSTRAINT [FK_PartAssemblies_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Assemblies] CHECK CONSTRAINT [FK_PartAssemblies_Customers]
GO
ALTER TABLE [dbo].[AssemblyVersionParts]  WITH CHECK ADD  CONSTRAINT [FK_AssemblyVersionParts_AssemblyVersions] FOREIGN KEY([AssemblyVersionId])
REFERENCES [dbo].[AssemblyVersions] ([AssemblyVersionId])
GO
ALTER TABLE [dbo].[AssemblyVersionParts] CHECK CONSTRAINT [FK_AssemblyVersionParts_AssemblyVersions]
GO
ALTER TABLE [dbo].[AssemblyVersionParts]  WITH CHECK ADD  CONSTRAINT [FK_AssemblyVersionParts_PartVersions] FOREIGN KEY([PartVersionId])
REFERENCES [dbo].[PartVersions] ([PartVersionId])
GO
ALTER TABLE [dbo].[AssemblyVersionParts] CHECK CONSTRAINT [FK_AssemblyVersionParts_PartVersions]
GO
ALTER TABLE [dbo].[AssemblyVersions]  WITH CHECK ADD  CONSTRAINT [FK_AssemblyVersions_Assemblies] FOREIGN KEY([AssemblyId])
REFERENCES [dbo].[Assemblies] ([AssemblyId])
GO
ALTER TABLE [dbo].[AssemblyVersions] CHECK CONSTRAINT [FK_AssemblyVersions_Assemblies]
GO
ALTER TABLE [dbo].[ContactDetails]  WITH CHECK ADD  CONSTRAINT [FK_ContactDetails_ContactDetailTypes] FOREIGN KEY([ContactDetailTypeId])
REFERENCES [dbo].[ContactDetailTypes] ([ContactDetailTypeId])
GO
ALTER TABLE [dbo].[ContactDetails] CHECK CONSTRAINT [FK_ContactDetails_ContactDetailTypes]
GO
ALTER TABLE [dbo].[CustomerAddresses]  WITH CHECK ADD  CONSTRAINT [FK_CustomerAddresses_Addresses] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Addresses] ([AddressId])
GO
ALTER TABLE [dbo].[CustomerAddresses] CHECK CONSTRAINT [FK_CustomerAddresses_Addresses]
GO
ALTER TABLE [dbo].[CustomerAddresses]  WITH CHECK ADD  CONSTRAINT [FK_CustomerAddresses_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerAddresses] CHECK CONSTRAINT [FK_CustomerAddresses_Customers]
GO
ALTER TABLE [dbo].[CustomerContactDetails]  WITH CHECK ADD  CONSTRAINT [FK_CustomerContactDetails_ContactDetails] FOREIGN KEY([ContactDetailId])
REFERENCES [dbo].[ContactDetails] ([ContactDetailId])
GO
ALTER TABLE [dbo].[CustomerContactDetails] CHECK CONSTRAINT [FK_CustomerContactDetails_ContactDetails]
GO
ALTER TABLE [dbo].[CustomerContactDetails]  WITH CHECK ADD  CONSTRAINT [FK_CustomerContactDetails_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerContactDetails] CHECK CONSTRAINT [FK_CustomerContactDetails_Customers]
GO
ALTER TABLE [dbo].[CustomerContacts]  WITH CHECK ADD  CONSTRAINT [FK_CustomerContacts_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerContacts] CHECK CONSTRAINT [FK_CustomerContacts_Customers]
GO
ALTER TABLE [dbo].[CustomerContacts]  WITH CHECK ADD  CONSTRAINT [FK_CustomerContacts_People] FOREIGN KEY([PersonId])
REFERENCES [dbo].[People] ([PersonId])
GO
ALTER TABLE [dbo].[CustomerContacts] CHECK CONSTRAINT [FK_CustomerContacts_People]
GO
ALTER TABLE [dbo].[Documents]  WITH NOCHECK ADD  CONSTRAINT [FK_Documents_DocumentFolders] FOREIGN KEY([DocumentFolderId])
REFERENCES [dbo].[DocumentFolders] ([DocumentFolderId])
GO
ALTER TABLE [dbo].[Documents] NOCHECK CONSTRAINT [FK_Documents_DocumentFolders]
GO
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD  CONSTRAINT [FK_Documents_DocumentTypes] FOREIGN KEY([DocumentTypeId])
REFERENCES [dbo].[DocumentTypes] ([DocumentTypeId])
GO
ALTER TABLE [dbo].[Documents] CHECK CONSTRAINT [FK_Documents_DocumentTypes]
GO
ALTER TABLE [dbo].[DocumentVersions]  WITH CHECK ADD  CONSTRAINT [FK_DocumentVersions_Documents] FOREIGN KEY([DocumentId])
REFERENCES [dbo].[Documents] ([DocumentId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DocumentVersions] CHECK CONSTRAINT [FK_DocumentVersions_Documents]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_EmployeeGroups] FOREIGN KEY([EmployeeGroupId])
REFERENCES [dbo].[EmployeeGroups] ([EmployeeGroupId])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_EmployeeGroups]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_People] FOREIGN KEY([PersonId])
REFERENCES [dbo].[People] ([PersonId])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_People]
GO
ALTER TABLE [dbo].[PartFixtures]  WITH CHECK ADD  CONSTRAINT [FK_PartFixtures_PartVersions] FOREIGN KEY([PartVersionId])
REFERENCES [dbo].[PartVersions] ([PartVersionId])
GO
ALTER TABLE [dbo].[PartFixtures] CHECK CONSTRAINT [FK_PartFixtures_PartVersions]
GO
ALTER TABLE [dbo].[Parts]  WITH CHECK ADD  CONSTRAINT [FK_Parts_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Parts] CHECK CONSTRAINT [FK_Parts_Customers]
GO
ALTER TABLE [dbo].[PartVersions]  WITH CHECK ADD  CONSTRAINT [FK_PartVersions_Parts] FOREIGN KEY([PartId])
REFERENCES [dbo].[Parts] ([PartId])
GO
ALTER TABLE [dbo].[PartVersions] CHECK CONSTRAINT [FK_PartVersions_Parts]
GO
ALTER TABLE [dbo].[PersonAddresses]  WITH CHECK ADD  CONSTRAINT [FK_PersonAddresses_Addresses] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Addresses] ([AddressId])
GO
ALTER TABLE [dbo].[PersonAddresses] CHECK CONSTRAINT [FK_PersonAddresses_Addresses]
GO
ALTER TABLE [dbo].[PersonAddresses]  WITH CHECK ADD  CONSTRAINT [FK_PersonAddresses_People] FOREIGN KEY([PersonId])
REFERENCES [dbo].[People] ([PersonId])
GO
ALTER TABLE [dbo].[PersonAddresses] CHECK CONSTRAINT [FK_PersonAddresses_People]
GO
ALTER TABLE [dbo].[PersonContactDetails]  WITH CHECK ADD  CONSTRAINT [FK_PersonContactDetails_ContactDetails] FOREIGN KEY([ContactDetailId])
REFERENCES [dbo].[ContactDetails] ([ContactDetailId])
GO
ALTER TABLE [dbo].[PersonContactDetails] CHECK CONSTRAINT [FK_PersonContactDetails_ContactDetails]
GO
ALTER TABLE [dbo].[PersonContactDetails]  WITH CHECK ADD  CONSTRAINT [FK_PersonContactDetails_People] FOREIGN KEY([PersonId])
REFERENCES [dbo].[People] ([PersonId])
GO
ALTER TABLE [dbo].[PersonContactDetails] CHECK CONSTRAINT [FK_PersonContactDetails_People]
GO
ALTER TABLE [dbo].[QuickMessages]  WITH CHECK ADD  CONSTRAINT [FK_QuickMessages_Employees_From] FOREIGN KEY([FromEmployeeId])
REFERENCES [dbo].[Employees] ([EmployeeId])
GO
ALTER TABLE [dbo].[QuickMessages] CHECK CONSTRAINT [FK_QuickMessages_Employees_From]
GO
ALTER TABLE [dbo].[QuickMessages]  WITH CHECK ADD  CONSTRAINT [FK_QuickMessages_Employees_To] FOREIGN KEY([ToEmployeeId])
REFERENCES [dbo].[Employees] ([EmployeeId])
GO
ALTER TABLE [dbo].[QuickMessages] CHECK CONSTRAINT [FK_QuickMessages_Employees_To]
GO
ALTER TABLE [dbo].[SupplierContacts]  WITH CHECK ADD  CONSTRAINT [FK_SupplierContacts_People] FOREIGN KEY([PersonId])
REFERENCES [dbo].[People] ([PersonId])
GO
ALTER TABLE [dbo].[SupplierContacts] CHECK CONSTRAINT [FK_SupplierContacts_People]
GO
ALTER TABLE [dbo].[SupplierContacts]  WITH CHECK ADD  CONSTRAINT [FK_SupplierContacts_Suppliers] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Suppliers] ([SupplierId])
GO
ALTER TABLE [dbo].[SupplierContacts] CHECK CONSTRAINT [FK_SupplierContacts_Suppliers]
GO
ALTER TABLE [dbo].[Suppliers]  WITH CHECK ADD  CONSTRAINT [FK_Suppliers_SupplierTypes] FOREIGN KEY([SupplierTypeId])
REFERENCES [dbo].[SupplierTypes] ([SupplierTypeId])
GO
ALTER TABLE [dbo].[Suppliers] CHECK CONSTRAINT [FK_Suppliers_SupplierTypes]
GO
USE [master]
GO
ALTER DATABASE [nGen_Production] SET  READ_WRITE 
GO
