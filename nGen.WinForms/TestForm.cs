﻿#region Using directives

using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using nGen.Data.EntityFramework.Model;
using nGen.Domain;
using nGen.Domain.Data.Model;
using nGen.Domain.IO;
using Security;

#endregion

namespace nGen.WinForms
{
    public partial class TestForm : Form
    {
        private readonly IDocumentService _documentService;
        private readonly IUnitOfWork _unitOfWork;

        public TestForm(IDocumentService documentService, IUnitOfWork unitOfWork)
        {
            InitializeComponent();

            _documentService = documentService;
            _unitOfWork = unitOfWork;

            _documentService.DocumentTransferProgressChanged += _documentService_DocumentTransferProgressChanged;
            _documentService.EncryptionProgressChanged += _documentService_EncryptionProgressChanged;
        }

        CancellableCallbackAction _documentService_EncryptionProgressChanged(string fileName, double progress)
        {
            if (InvokeRequired)
            {
                this.InvokeEx(() =>
                {
                    label1.Text = "decrypting document...";
                    progressBar1.Value = Convert.ToInt32(progress);
                });

                return CancellableCallbackAction.Continue;
            }

            return CancellableCallbackAction.Continue;
        }

        CancellableCallbackAction _documentService_DocumentTransferProgressChanged(string fileName, string status, double progress)
        {
            if (InvokeRequired) {
                this.InvokeEx(() => {
                    label1.Text = fileName;
                    label2.Text = status;
                    progressBar1.Value = Convert.ToInt32(progress);
                });

                return CancellableCallbackAction.Continue;
            }
            
            return CancellableCallbackAction.Continue;
        }

        private void TestForm_Load(object sender, EventArgs e)
        {

        }

        #region Nested type: Node

        private class Node
        {
            private readonly IEntity _entity;

            public Node(IEntity entity)
            {
                if (!(entity is ICustomer) && !(entity is IPart)) {
                    throw new ArgumentException("Entity must be customer or part!", "entity");
                }

                _entity = entity;
            }

            public bool IsCustomer
            {
                get { return _entity is ICustomer; }
            }

            public IEntity Entity
            {
                get { return _entity; }
            }
        }

        #endregion

        private async void button1_Click(object sender, EventArgs e)
        {
            var doc = _unitOfWork.Documents.GetByPK(2);

            await _documentService.DecryptAsync(doc);
        }
    }
}