﻿#region Using directives

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.IO;
using nGen.Domain.Security;
using nGen.WinForms.IO;
using Security;

#endregion

namespace nGen.WinForms
{
    internal static class Program
    {
        #region Win32 API

        private const int SW_RESTORE = 9;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern Boolean ShowWindow(IntPtr hWnd, Int32 nCmdShow);

        #endregion

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            var createdNew = true;

            // ensure only one instance of the application can be open at a time
            using (var mutex = new Mutex(true, "nGen.WinForms", out createdNew)) {
                if (createdNew) {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);

                    Session.Initialize();

                    var mainForm = Session.GetInstance<MainForm>();

                    Application.Run(mainForm);
                }
                else {
                    var current = Process.GetCurrentProcess();
                    foreach (var process in Process.GetProcessesByName(current.ProcessName)) {
                        if (process.Id != current.Id) {
                            ShowWindow(process.MainWindowHandle, SW_RESTORE);
                            SetForegroundWindow(process.MainWindowHandle);
                            break;
                        }
                    }
                }
            }
        }

        private static void CreateAdminAccount()
        {
            var now = DateTime.Now;

            var entities = new nGenEntities();

            var person =
                entities.People.SingleOrDefault(p => p.FirstName == "System" && p.LastName == "Administrator");

            if (person == null) {
                #region Create person

                person = new Person {
                    FirstName = "System",
                    LastName = "Administrator",
                    DateOfBirth = now,
                    CreatedOn = now,
                    ModifiedOn = now,
                    CreatedBy = 0,
                    ModifiedBy = 0
                };

                entities.People.Add(person);

                entities.SaveChanges();

                #endregion
            }

            var group = entities.EmployeeGroups.SingleOrDefault(g => g.Name == "BUILTIN_ADMIN");

            if (group == null) {
                #region Create employee group

                var permissions = new AppPermissions();
                permissions.GrantPermission(AppPermission.IsAdmin);

                group = new EmployeeGroup {
                    Name = "BUILTIN_ADMIN",
                    Description = "Built in administrator account with full access to the system",
                    Permissions = permissions.SerializeAndEncrypt(),
                    CreatedOn = now,
                    ModifiedOn = now,
                    CreatedBy = 0,
                    ModifiedBy = 0
                };

                entities.EmployeeGroups.Add(group);

                entities.SaveChanges();

                permissions = AppPermissions.DecryptAndDeserialize(group.Permissions);
                permissions.EmployeeGroupId = group.EmployeeGroupId;
                group.Permissions = permissions.SerializeAndEncrypt();

                entities.SaveChanges();

                #endregion
            }

            var employee = entities.Employees.SingleOrDefault(e => e.UserName == "admin");

            if (employee == null) {
                #region Create employee

                var pwd = Session.GetInstance<IPasswordService>().EncryptPassword("Tk3jF89FzN4K2Nh9");

                employee = new Employee {
                    UserName = "admin",
                    Person = person,
                    EmployeeGroup = group,
                    Password = pwd.Hash,
                    Salt = pwd.Salt,
                    CreatedOn = now,
                    ModifiedOn = now,
                    CreatedBy = 0,
                    ModifiedBy = 0
                };

                entities.Employees.Add(employee);

                entities.SaveChanges();

                person.CreatedBy = employee.EmployeeId;
                person.ModifiedBy = employee.EmployeeId;

                group.CreatedBy = employee.EmployeeId;
                group.ModifiedBy = employee.EmployeeId;

                employee.CreatedBy = employee.EmployeeId;
                employee.ModifiedBy = employee.EmployeeId;

                entities.SaveChanges();

                #endregion
            }
        }
    }
}