﻿#region Using directives

using System.Windows.Forms;
using nGen.Domain.Dialogs;
using nGen.WinForms.Properties;

#endregion

namespace nGen.WinForms
{
    public partial class BaseForm : Form
    {
        protected readonly IDialogService DialogService;

        protected BaseForm()
        {
        }

        protected BaseForm(IDialogService dialogService)
        {
            InitializeComponent();
            DialogService = dialogService;
            base.Font = Settings.Default.ApplicationFont;
        }
    }
}