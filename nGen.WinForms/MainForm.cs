﻿#region Using directives

using System;
using System.Linq;
using System.Windows.Forms;
using nGen.Domain.Data.Model;
using nGen.Domain.Dialogs;
using nGen.WinForms.Properties;

#endregion

namespace nGen.WinForms
{
    public partial class MainForm : BaseForm
    {
        public MainForm(IDialogService dialogService) : base(dialogService)
        {
            InitializeComponent();

            base.Font = Settings.Default.ApplicationFont;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            using (var uow = Session.GetInstance<IUnitOfWork>()) {
                var customer = uow.Customers.GetAll(true).First();
                MessageBox.Show(customer.Name);
            }
        }
    }
}