﻿#region Using directives

using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using nGen.Domain;
using nGen.Domain.Data.Model;
using nGen.Domain.IO;
using nGen.Domain.Security;
using Security;

#endregion

namespace nGen.WinForms.IO
{
    public sealed class LocalDocumentService : IDocumentService
    {
        private readonly IUnitOfWork _unitOfWork;

        public LocalDocumentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #region IDocumentService Members

        public event DocumentTransferCallback DocumentTransferProgressChanged;

        public event EncryptionProgressCallback EncryptionProgressChanged;

        private CancellableCallbackAction OnEncryptionProgressChanged(string filename, double progress)
        {
            EncryptionProgressCallback handler = EncryptionProgressChanged;
            if (handler != null) {
                return handler(filename, progress);
            }
            return CancellableCallbackAction.Continue;
        }

        public async Task UploadAsync(string fileToUpload, IDocumentType documentType, DocumentPermissions permissions, ICustomer customer)
        {
            var fileName = Path.GetFileName(fileToUpload);

            var document = _unitOfWork.Documents.GetCustomerDocuments(customer)
                .SingleOrDefault(d => d.FileName.Equals(fileName, StringComparison.OrdinalIgnoreCase));

            if (document == null) {
                document = Session.GetInstance<IDocument>();
                document.FileName = fileName;
                document.CustomerId = customer.CustomerId;
                document.DocumentTypeId = documentType.DocumentTypeId;
                document.CreatedOn = DateTime.Now;
                document.ModifiedOn = DateTime.Now;
                document.CreatedBy = Session.CurrentEmployeeId;
                document.ModifiedBy = Session.CurrentEmployeeId;
                document.Permissions = permissions.SerializeAndEncrypt();
            }

            await UploadAsync(document, fileToUpload);
        }

        public async Task UploadAsync(string fileToUpload, IDocumentType documentType, DocumentPermissions permissions, IDocumentFolder documentFolder)
        {
            var fileName = Path.GetFileName(fileToUpload);

            var document = _unitOfWork.Documents.GetFolderDocuments(documentFolder)
                .SingleOrDefault(d => d.FileName.Equals(fileName, StringComparison.OrdinalIgnoreCase));

            if (document == null) {
                document = Session.GetInstance<IDocument>();
                document.FileName = fileName;
                document.DocumentFolderId = documentFolder.DocumentFolderId;
                document.DocumentTypeId = documentType.DocumentTypeId;
                document.CreatedOn = DateTime.Now;
                document.ModifiedOn = DateTime.Now;
                document.CreatedBy = Session.CurrentEmployeeId;
                document.ModifiedBy = Session.CurrentEmployeeId;
                document.Permissions = permissions.SerializeAndEncrypt();
            }

            await UploadAsync(document, fileToUpload);
        }

        public async Task UploadAsync(string fileToUpload, IDocumentType documentType, DocumentPermissions permissions, IPart part)
        {
            var fileName = Path.GetFileName(fileToUpload);

            var document = _unitOfWork.Documents.GetPartDocuments(part)
                .SingleOrDefault(d => d.FileName.Equals(fileName, StringComparison.OrdinalIgnoreCase));

            if (document == null) {
                document = Session.GetInstance<IDocument>();
                document.FileName = fileName;
                document.PartId = part.PartId;
                document.DocumentTypeId = documentType.DocumentTypeId;
                document.CreatedOn = DateTime.Now;
                document.ModifiedOn = DateTime.Now;
                document.CreatedBy = Session.CurrentEmployeeId;
                document.ModifiedBy = Session.CurrentEmployeeId;
                document.Permissions = permissions.SerializeAndEncrypt();
            }

            await UploadAsync(document, fileToUpload);
        }

        public async Task UploadAsync(string fileToUpload, IDocumentType documentType, DocumentPermissions permissions, IPartVersion partVersion)
        {
            var fileName = Path.GetFileName(fileToUpload);

            var document = _unitOfWork.Documents.GetPartVersionDocuments(partVersion)
                .SingleOrDefault(d => d.FileName.Equals(fileName, StringComparison.OrdinalIgnoreCase));

            if (document == null) {
                document = Session.GetInstance<IDocument>();
                document.FileName = fileName;
                document.PartVersionId = partVersion.PartVersionId;
                document.DocumentTypeId = documentType.DocumentTypeId;
                document.CreatedOn = DateTime.Now;
                document.ModifiedOn = DateTime.Now;
                document.CreatedBy = Session.CurrentEmployeeId;
                document.ModifiedBy = Session.CurrentEmployeeId;
                document.Permissions = permissions == null ? null : permissions.SerializeAndEncrypt();
            }

            await UploadAsync(document, fileToUpload);
        }

        public async Task EncryptAsync(IDocument document)
        {
            var fileEncrypter = new FileEncrypter();

            fileEncrypter.EncryptionProgressChanged += OnEncryptionProgressChanged;

            var allVersions = _unitOfWork.DocumentVersions.GetAllVersions(document);

            var clientSettings = _unitOfWork.ClientSettings.GetClientSettings();
            
            var encryptionPassword = new SymmetricCryptoProvider().DecryptString(clientSettings.EncryptionPassword);

            foreach (var version in allVersions) {
                var fileName = GetPathToVersion(version);

                await fileEncrypter.EncryptFileAsync(fileName, encryptionPassword);
            }
        }

        public async Task DecryptAsync(IDocument document)
        {
            var fileEncrypter = new FileEncrypter();

            fileEncrypter.EncryptionProgressChanged += OnEncryptionProgressChanged;

            var allVersions = _unitOfWork.DocumentVersions.GetAllVersions(document);

            var clientSettings = _unitOfWork.ClientSettings.GetClientSettings();

            var encryptionPassword = new SymmetricCryptoProvider().DecryptString(clientSettings.EncryptionPassword);

            foreach (var version in allVersions)
            {
                var fileName = GetPathToVersion(version);

                await fileEncrypter.DecryptFileAsync(fileName, encryptionPassword);
            }
        }

        public async Task OpenReadOnlyAsync(IDocument document)
        {
            throw new NotImplementedException();
        }

        public async Task CheckOutAsync(IDocument document)
        {
            throw new NotImplementedException();
        }

        public async Task CheckInAsync(IDocument document)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(IDocument document)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(IDocumentVersion documentVersion)
        {
            throw new NotImplementedException();
        }

        public async Task RenameAsync(IDocument document, string newName)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private methods

        private async Task UploadAsync(IDocument document, string fileToUpload)
        {
            OnDocumentTransferProgressChanged(fileToUpload, "preparing", 0);

            var latestVersion = _unitOfWork.DocumentVersions.GetLatestVersion(document);

            if (latestVersion == null) {
                // is new document
                _unitOfWork.Documents.Add(document);
                _unitOfWork.Commit();
            }

            await CreateNewRevisionAsync(document, fileToUpload);
        }

        private async Task CreateNewRevisionAsync(IDocument document, string sourceFile)
        {
            OnDocumentTransferProgressChanged(sourceFile, "computing MD5 hash", 0);

            var hash = await ComputeFileHashAsync(sourceFile);

            var latestVersion = _unitOfWork.DocumentVersions.GetLatestVersion(document);

            if (latestVersion != null) {
                if (latestVersion.MD5Hash == hash) {
                    OnDocumentTransferProgressChanged(sourceFile, "document hasn't been modified", 100);
                    return;
                }
            }

            var newVersion = Session.GetInstance<IDocumentVersion>();
            newVersion.DocumentId = document.DocumentId;
            newVersion.MD5Hash = hash;
            newVersion.CreatedBy = Session.CurrentEmployeeId;
            newVersion.CreatedOn = DateTime.Now;

            _unitOfWork.DocumentVersions.Add(newVersion);
            _unitOfWork.Commit();

            var clientSettings = _unitOfWork.ClientSettings.GetClientSettings();

            IEntity entity = null;
            if (document.CustomerId.HasValue) {
                entity = _unitOfWork.Customers.GetByPK(document.CustomerId.Value);
            }
            else if (document.DocumentFolderId.HasValue) {
                entity = _unitOfWork.DocumentFolders.GetByPK(document.DocumentFolderId.Value);
            }
            else if (document.PartId.HasValue) {
                entity = _unitOfWork.Parts.GetByPK(document.PartId.Value);
            }
            else if (document.PartVersionId.HasValue) {
                entity = _unitOfWork.PartVersions.GetByPK(document.PartVersionId.Value);
            }

            var storageDir = Path.Combine(clientSettings.WindowsDataDir, "Repo", GetStorageDirectory(entity));

            if (!Directory.Exists(storageDir)) {
                Directory.CreateDirectory(storageDir);
            }

            var destinationFile = GetPathToVersion(newVersion);

            var fileCopier = new FileCopier();
            fileCopier.TransferProgress += (fileName, status, progress) => OnDocumentTransferProgressChanged(fileName, "copying", progress);
            await fileCopier.CopyFileAsync(sourceFile, destinationFile);

            var allVersions = _unitOfWork.DocumentVersions.GetAllVersions(document);

            if (allVersions.Count > clientSettings.MaxDocumentVersionCount)
            {
                var oldestVersion = allVersions.OrderBy(ver => ver.CreatedOn).First();
                await DeleteAsync(oldestVersion);
            }
        }

        /// <summary>
        ///     Computes the MD5 hash of the specified file and returns a 32 character string representation of it
        /// </summary>
        /// <param name="fileName">The file to compute the MD5 hash for</param>
        /// <returns></returns>
        private async Task<string> ComputeFileHashAsync(string fileName)
        {
            const int bufferSize = 1048576;

            return await Task.Run(() => {
                // wrap in BufferedStream with 1MB buffer as it increases hashing speed for large files
                // source: http://stackoverflow.com/questions/1177607/what-is-the-fastest-way-to-create-a-checksum-for-large-files-in-c-sharp
                using (var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None)) {
                    using (var bufferedStream = new BufferedStream(fileStream, bufferSize)) {
                        var md5 = new MD5CryptoServiceProvider();
                        var checksum = md5.ComputeHash(bufferedStream);
                        return BitConverter.ToString(checksum).Replace("-", String.Empty);
                    }
                }
            });
        }

        /// <summary>
        ///     Gets the path in which to store documents for the specified entity. Does not include the base directory
        ///     as this could be either the document or check-out storage location.
        ///     Valid types are <see cref="ICustomer" />, <see cref="IDocumentFolder" />, <see cref="IPart" /> and
        ///     <see cref="IPartVersion" />
        /// </summary>
        /// <param name="entity">The entity to get the storage directory for</param>
        /// <returns>The directory in which to store files for the specified entity</returns>
        /// <exception cref="ArgumentException">Thrown when the wrong type of entity is provided</exception>
        private string GetStorageDirectory(IEntity entity)
        {
            if (entity is ICustomer) {
                var customer = entity as ICustomer;
                return string.Format("Customer_{0}\\", customer.CustomerId);
            }

            if (entity is IPart) {
                var part = entity as IPart;
                return string.Format("Customer_{0}\\Part_{1}\\", part.CustomerId, part.PartId);
            }

            if (entity is IPartVersion) {
                var version = entity as IPartVersion;
                var customerId = _unitOfWork.Parts.GetByPK(version.PartId).CustomerId;
                return string.Format("Customer_{0}\\Part_{1}\\PartVersion_{2}\\", customerId, version.PartId, version.PartVersionId);
            }

            if (entity is IDocumentFolder) {
                var folder = entity as IDocumentFolder;

                string dir = null;

                if (folder.ParentFolderId.HasValue) {
                    // get the parent folder
                    var parentFolder = _unitOfWork.DocumentFolders.GetByPK(folder.ParentFolderId.Value);

                    // get the path to the parent folder
                    dir = GetStorageDirectory(parentFolder);
                }
                else if (folder.PartId.HasValue) {
                    var part = _unitOfWork.Parts.GetByPK(folder.PartId.Value);

                    dir = GetStorageDirectory(part);
                }

                return string.Format("{0}\\Folder_{1}\\", dir, folder.DocumentFolderId);
            }

            // if we get here, the wrong type of entity has been provided
            throw new ArgumentException("Cannot get a directory for this entity type: " + entity.GetType().FullName,
                "entity");
        }

        private string GetPathToVersion(IDocumentVersion version)
        {
            var document = _unitOfWork.Documents.GetByPK(version.DocumentId);

            IEntity entity= null;

            if (document.CustomerId.HasValue) {
                entity = _unitOfWork.Customers.GetByPK(document.CustomerId.Value);
            }
            else if (document.DocumentFolderId.HasValue) {
                entity = _unitOfWork.DocumentFolders.GetByPK(document.DocumentFolderId.Value);
            }
            else if (document.PartId.HasValue) {
                entity = _unitOfWork.Parts.GetByPK(document.PartId.Value);
            }
            else if (document.PartVersionId.HasValue) {
                entity = _unitOfWork.PartVersions.GetByPK(document.PartVersionId.Value);
            }

            var dataDir = _unitOfWork.ClientSettings.GetClientSettings().WindowsDataDir;

            var entityDir = GetStorageDirectory(entity);

            return string.Format("{0}\\{1}\\{2}\\Version_{3}", dataDir, "Repo", entityDir, version.DocumentVersionId);
        }

        private CancellableCallbackAction OnDocumentTransferProgressChanged(string fileName, string status,
            double progress)
        {
            var handler = DocumentTransferProgressChanged;

            if (handler != null)
            {
                return handler(fileName, status, progress);
            }

            return CancellableCallbackAction.Continue;
        }

        #endregion
    }
}