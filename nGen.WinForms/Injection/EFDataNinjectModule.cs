﻿#region Using directives

using nGen.Data.EntityFramework.Model;
using nGen.Data.EntityFramework.Repositories;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;
using Ninject.Modules;

#endregion

namespace nGen.WinForms.Injection
{
    public class EFDataNinjectModule : NinjectModule
    {
        public override void Load()
        {
            // bind the model
            Bind<IClientSetting>().To<ClientSetting>();
            Bind<ICustomer>().To<Customer>();
            Bind<IDocument>().To<Document>();
            Bind<IDocumentType>().To<DocumentType>();
            Bind<IDocumentVersion>().To<DocumentVersion>();
            Bind<IEmployee>().To<Employee>();
            Bind<IEmployeeGroup>().To<EmployeeGroup>();
            Bind<IPart>().To<Part>();
            Bind<IPartVersion>().To<PartVersion>();
            Bind<IPerson>().To<Person>();

            // bind the repositories
            Bind<IClientSettingRepository>().To<EFClientSettingRepository>();
            Bind<ICustomerRepository>().To<EFCustomerRepository>();
            Bind<IDocumentRepository>().To<EFDocumentRepository>();
            Bind<IEmployeeRepository>().To<EFEmployeeRepository>();
            Bind<IEmployeeGroupRepository>().To<EFEmployeeGroupRepository>();
            Bind<IPartRepository>().To<EFPartRepository>();
            Bind<IPartVersionRepository>().To<EFPartVersionRepository>();
            Bind<IPersonRepository>().To<EFPersonRepository>();

            // bind the unit of work
            Bind<IUnitOfWork>().To<EFUnitOfWork>();
        }
    }
}