﻿#region Using directives

using nGen.Domain.Dialogs;
using nGen.Domain.IO;
using nGen.Domain.Security;
using nGen.WinForms.Dialogs;
using nGen.WinForms.IO;
using Ninject.Modules;

#endregion

namespace nGen.WinForms.Injection
{
    internal sealed class ServiceNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDialogService>().To<SimpleDialogService>();
            Bind<IDocumentService>().To<LocalDocumentService>();
            Bind<IPasswordService>().To<PBKDF2PasswordService>();
        }
    }
}