﻿#region Using directives

using System.Windows.Forms;
using nGen.Domain.Dialogs;

#endregion

namespace nGen.WinForms.Dialogs
{
    public sealed class SimpleDialogService : IDialogService
    {
        #region IDialogService Members

        public DialogServiceResult AskQuestion(string question)
        {
            return AskQuestion(question, false);
        }

        public DialogServiceResult AskQuestion(string question, bool canCancel)
        {
            var result = MessageBox.Show(
                question,
                "Question",
                canCancel ? MessageBoxButtons.YesNoCancel : MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            switch (result) {
                case DialogResult.Yes:
                    return DialogServiceResult.Yes;
                case DialogResult.No:
                    return DialogServiceResult.No;
                case DialogResult.Cancel:
                    return DialogServiceResult.Cancel;
                default:
                    return DialogServiceResult.Cancel;
            }
        }

        public void Notify(string message)
        {
            MessageBox.Show(message, "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion
    }
}