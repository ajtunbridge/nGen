﻿#region Using directives

using System.Drawing;
using System.Windows.Forms;
using nGen.WinForms.Injection;
using nGen.WinForms.Properties;
using Ninject;
using Ninject.Modules;

#endregion

namespace nGen.WinForms
{
    internal static class Session
    {
        private static StandardKernel _kernel;

        /// <summary>
        ///     The ID value of the employee currently logged into the system
        /// </summary>
        internal static int CurrentEmployeeId { get; set; }

        internal static void Initialize()
        {
            // determine the application font based on screen resolution
            var resolution = Screen.PrimaryScreen.Bounds;

            var currentFont = Settings.Default.ApplicationFont;

            var fontSize = resolution.Height <= 768 ? 8f : 10f;

            Settings.Default.ApplicationFont = new Font(currentFont.FontFamily, fontSize);

            Settings.Default.Save();

            // setup dependency injection
            _kernel = new StandardKernel(new INinjectModule[] {
                new EFDataNinjectModule(),
                new ServiceNinjectModule()
            });
        }

        internal static T GetInstance<T>()
        {
            return _kernel.Get<T>();
        }
    }
}