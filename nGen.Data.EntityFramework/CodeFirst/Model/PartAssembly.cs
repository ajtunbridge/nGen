﻿#region Using directives

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace nGen.Data.EntityFramework.CodeFirst.Model
{
    [Table("PartAssemblies")]
    public class PartAssembly
    {
        public PartAssembly()
        {
        }

        public int PartAssemblyId { get; set; }

        [Required]
        [StringLength(50)]
        public string DrawingNumber { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public Customer Customer { get; set; }
    }
}