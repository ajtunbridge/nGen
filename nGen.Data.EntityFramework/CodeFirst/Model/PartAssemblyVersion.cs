﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nGen.Data.EntityFramework.CodeFirst.Model
{
    public class PartAssemblyVersion
    {
        public PartAssemblyVersion()
        {
            
        }

        public int PartAssemblyVersionId { get; set; }

        [Required]
        [StringLength(10)]
        public string VersionNumber { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public PartAssembly PartAssembly { get; set; }
    }
}