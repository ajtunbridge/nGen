﻿#region Using directives

using System.ComponentModel.DataAnnotations;

#endregion

namespace nGen.Data.EntityFramework.CodeFirst.Model
{
    public sealed class Address
    {
        public Address()
        {
        }

        public int AddressId { get; set; }

        [Required]
        [StringLength(100)]
        public string Line1 { get; set; }

        [StringLength(100)]
        public string Line2 { get; set; }

        [Required]
        [StringLength(100)]
        public string City { get; set; }

        [Required]
        [StringLength(100)]
        public string Country { get; set; }

        [Required]
        [StringLength(12)]
        public string Postcode { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public AddressType AddressType { get; set; }
    }
}