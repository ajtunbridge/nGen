﻿#region Using directives

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace nGen.Data.EntityFramework.CodeFirst.Model
{
    [Table("AddressTypes")]
    public class AddressType
    {
        public int AddressTypeId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}