﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nGen.Data.EntityFramework.CodeFirst.Model
{
    public class Customer
    {
        public Customer()
        {
        }

        public int CustomerId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
