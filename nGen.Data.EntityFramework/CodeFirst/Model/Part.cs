﻿#region Using directives

using System.ComponentModel.DataAnnotations;

#endregion

namespace nGen.Data.EntityFramework.CodeFirst.Model
{
    public class Part
    {
        public Part()
        {
            // set default values
            Item = 1;
        }

        public int PartId { get; set; }

        [Required]
        [StringLength(50)]
        public string DrawingNumber { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public int Item { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public Customer Customer { get; set; }
    }
}