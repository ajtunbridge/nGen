﻿#region Using directives

using System.ComponentModel.DataAnnotations;

#endregion

namespace nGen.Data.EntityFramework.CodeFirst.Model
{
    public class PartVersion
    {
        public PartVersion()
        {
        }

        [Required]
        [StringLength(10)]
        public string VersionNumber { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public Part Part { get; set; }
    }
}