﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nGen.Data.EntityFramework.CodeFirst.Model
{
    public class nGenContext : DbContext
    {
        public nGenContext() : base()
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Address>().MapToStoredProcedures();
            modelBuilder.Entity<AddressType>().MapToStoredProcedures();
            modelBuilder.Entity<Customer>().MapToStoredProcedures();
            modelBuilder.Entity<Part>().MapToStoredProcedures();
            modelBuilder.Entity<PartAssembly>().MapToStoredProcedures();
            modelBuilder.Entity<PartVersion>().MapToStoredProcedures();
        }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<AddressType> AddressTypes { get;set }
    }
}
