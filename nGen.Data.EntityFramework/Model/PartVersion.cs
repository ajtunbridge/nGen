//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace nGen.Data.EntityFramework.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class PartVersion
    {
        public PartVersion()
        {
            this.AssemblyVersionParts = new HashSet<AssemblyVersionPart>();
            this.PartFixtures = new HashSet<PartFixture>();
        }
    
        public int PartVersionId { get; set; }
        public string VersionNumber { get; set; }
        public byte[] NotesBytes { get; set; }
        public byte[] PhotoBytes { get; set; }
        public int PartId { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    
        public virtual ICollection<AssemblyVersionPart> AssemblyVersionParts { get; set; }
        public virtual ICollection<PartFixture> PartFixtures { get; set; }
        public virtual Part Part { get; set; }
    }
}
