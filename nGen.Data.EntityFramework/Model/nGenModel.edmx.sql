
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/30/2014 21:43:10
-- Generated from EDMX file: S:\Adam\Documents\GitHub\nGen\nGen.Data.EntityFramework\Model\nGenModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [nGen_Production];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Documents_DocumentFolders]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [FK_Documents_DocumentFolders];
GO
IF OBJECT_ID(N'[dbo].[FK_Documents_DocumentTypes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [FK_Documents_DocumentTypes];
GO
IF OBJECT_ID(N'[dbo].[FK_DocumentVersions_Documents]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DocumentVersions] DROP CONSTRAINT [FK_DocumentVersions_Documents];
GO
IF OBJECT_ID(N'[dbo].[FK_Employees_EmployeeGroups]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_EmployeeGroups];
GO
IF OBJECT_ID(N'[dbo].[FK_Employees_People]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_People];
GO
IF OBJECT_ID(N'[dbo].[FK_PartFixtures_Parts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PartFixtures] DROP CONSTRAINT [FK_PartFixtures_Parts];
GO
IF OBJECT_ID(N'[dbo].[FK_Parts_Customers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Parts] DROP CONSTRAINT [FK_Parts_Customers];
GO
IF OBJECT_ID(N'[dbo].[FK_PartVersions_Parts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PartVersions] DROP CONSTRAINT [FK_PartVersions_Parts];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ClientSettings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClientSettings];
GO
IF OBJECT_ID(N'[dbo].[Customers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Customers];
GO
IF OBJECT_ID(N'[dbo].[DocumentFolders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DocumentFolders];
GO
IF OBJECT_ID(N'[dbo].[Documents]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Documents];
GO
IF OBJECT_ID(N'[dbo].[DocumentTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DocumentTypes];
GO
IF OBJECT_ID(N'[dbo].[DocumentVersions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DocumentVersions];
GO
IF OBJECT_ID(N'[dbo].[EmployeeGroups]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EmployeeGroups];
GO
IF OBJECT_ID(N'[dbo].[Employees]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Employees];
GO
IF OBJECT_ID(N'[dbo].[PartFixtures]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PartFixtures];
GO
IF OBJECT_ID(N'[dbo].[Parts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Parts];
GO
IF OBJECT_ID(N'[dbo].[PartVersions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PartVersions];
GO
IF OBJECT_ID(N'[dbo].[People]', 'U') IS NOT NULL
    DROP TABLE [dbo].[People];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Customers'
CREATE TABLE [dbo].[Customers] (
    [CustomerId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [Terms] nvarchar(50)  NULL,
    [VATNumber] nvarchar(20)  NULL,
    [IsApproved] bit  NOT NULL,
    [TaxCode] nvarchar(2)  NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- Creating table 'DocumentTypes'
CREATE TABLE [dbo].[DocumentTypes] (
    [DocumentTypeId] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(30)  NOT NULL,
    [Description] varchar(255)  NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- Creating table 'EmployeeGroups'
CREATE TABLE [dbo].[EmployeeGroups] (
    [EmployeeGroupId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(255)  NULL,
    [Permissions] varbinary(max)  NOT NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- Creating table 'Employees'
CREATE TABLE [dbo].[Employees] (
    [EmployeeId] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(30)  NOT NULL,
    [Password] char(88)  NOT NULL,
    [Salt] char(24)  NOT NULL,
    [PersonId] int  NOT NULL,
    [EmployeeGroupId] int  NOT NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- Creating table 'PartFixtures'
CREATE TABLE [dbo].[PartFixtures] (
    [PartFixtureId] int IDENTITY(1,1) NOT NULL,
    [Location] varchar(30)  NOT NULL,
    [Contents] varchar(max)  NULL,
    [PhotoBytes] varbinary(max)  NULL,
    [PartId] int  NOT NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- Creating table 'Parts'
CREATE TABLE [dbo].[Parts] (
    [PartId] int IDENTITY(1,1) NOT NULL,
    [DrawingNumber] varchar(50)  NOT NULL,
    [Name] varchar(100)  NULL,
    [IsMainAssembly] bit  NOT NULL,
    [AssemblyParentId] int  NULL,
    [CustomerId] int  NOT NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- Creating table 'People'
CREATE TABLE [dbo].[People] (
    [PersonId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(50)  NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [MiddleName] nvarchar(50)  NULL,
    [LastName] nvarchar(50)  NOT NULL,
    [Suffix] nvarchar(50)  NULL,
    [NickName] nvarchar(50)  NULL,
    [DateOfBirth] datetime  NULL,
    [PhotoBytes] varbinary(max)  NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- Creating table 'PartVersions'
CREATE TABLE [dbo].[PartVersions] (
    [PartVersionId] int IDENTITY(1,1) NOT NULL,
    [VersionNumber] varchar(10)  NOT NULL,
    [PhotoBytes] varbinary(max)  NULL,
    [PartId] int  NOT NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- Creating table 'DocumentFolders'
CREATE TABLE [dbo].[DocumentFolders] (
    [DocumentFolderId] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(50)  NOT NULL,
    [ParentFolderId] int  NULL,
    [CustomerId] int  NULL,
    [PartId] int  NULL,
    [PartVersionId] int  NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- Creating table 'Documents'
CREATE TABLE [dbo].[Documents] (
    [DocumentId] int IDENTITY(1,1) NOT NULL,
    [FileName] varchar(100)  NOT NULL,
    [IsEncrypted] bit  NOT NULL,
    [Permissions] varbinary(max)  NULL,
    [DocumentTypeId] int  NOT NULL,
    [CustomerId] int  NULL,
    [DocumentFolderId] int  NULL,
    [PartId] int  NULL,
    [PartVersionId] int  NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- Creating table 'DocumentVersions'
CREATE TABLE [dbo].[DocumentVersions] (
    [DocumentVersionId] int IDENTITY(1,1) NOT NULL,
    [MD5Hash] char(32)  NOT NULL,
    [DocumentId] int  NOT NULL,
    [CreatedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL
);
GO

-- Creating table 'ClientSettings'
CREATE TABLE [dbo].[ClientSettings] (
    [ClientSettingId] int IDENTITY(1,1) NOT NULL,
    [WindowsDataDir] varchar(255)  NOT NULL,
    [EncryptionPassword] varchar(64)  NOT NULL,
    [MaximumVersionCount] tinyint  NOT NULL,
    [CreatedOn] datetime  NOT NULL,
    [ModifiedOn] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedBy] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [CustomerId] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [PK_Customers]
    PRIMARY KEY CLUSTERED ([CustomerId] ASC);
GO

-- Creating primary key on [DocumentTypeId] in table 'DocumentTypes'
ALTER TABLE [dbo].[DocumentTypes]
ADD CONSTRAINT [PK_DocumentTypes]
    PRIMARY KEY CLUSTERED ([DocumentTypeId] ASC);
GO

-- Creating primary key on [EmployeeGroupId] in table 'EmployeeGroups'
ALTER TABLE [dbo].[EmployeeGroups]
ADD CONSTRAINT [PK_EmployeeGroups]
    PRIMARY KEY CLUSTERED ([EmployeeGroupId] ASC);
GO

-- Creating primary key on [EmployeeId] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [PK_Employees]
    PRIMARY KEY CLUSTERED ([EmployeeId] ASC);
GO

-- Creating primary key on [PartFixtureId] in table 'PartFixtures'
ALTER TABLE [dbo].[PartFixtures]
ADD CONSTRAINT [PK_PartFixtures]
    PRIMARY KEY CLUSTERED ([PartFixtureId] ASC);
GO

-- Creating primary key on [PartId] in table 'Parts'
ALTER TABLE [dbo].[Parts]
ADD CONSTRAINT [PK_Parts]
    PRIMARY KEY CLUSTERED ([PartId] ASC);
GO

-- Creating primary key on [PersonId] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [PK_People]
    PRIMARY KEY CLUSTERED ([PersonId] ASC);
GO

-- Creating primary key on [PartVersionId] in table 'PartVersions'
ALTER TABLE [dbo].[PartVersions]
ADD CONSTRAINT [PK_PartVersions]
    PRIMARY KEY CLUSTERED ([PartVersionId] ASC);
GO

-- Creating primary key on [DocumentFolderId] in table 'DocumentFolders'
ALTER TABLE [dbo].[DocumentFolders]
ADD CONSTRAINT [PK_DocumentFolders]
    PRIMARY KEY CLUSTERED ([DocumentFolderId] ASC);
GO

-- Creating primary key on [DocumentId] in table 'Documents'
ALTER TABLE [dbo].[Documents]
ADD CONSTRAINT [PK_Documents]
    PRIMARY KEY CLUSTERED ([DocumentId] ASC);
GO

-- Creating primary key on [DocumentVersionId] in table 'DocumentVersions'
ALTER TABLE [dbo].[DocumentVersions]
ADD CONSTRAINT [PK_DocumentVersions]
    PRIMARY KEY CLUSTERED ([DocumentVersionId] ASC);
GO

-- Creating primary key on [ClientSettingId] in table 'ClientSettings'
ALTER TABLE [dbo].[ClientSettings]
ADD CONSTRAINT [PK_ClientSettings]
    PRIMARY KEY CLUSTERED ([ClientSettingId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CustomerId] in table 'Parts'
ALTER TABLE [dbo].[Parts]
ADD CONSTRAINT [FK_Parts_Customers]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[Customers]
        ([CustomerId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Parts_Customers'
CREATE INDEX [IX_FK_Parts_Customers]
ON [dbo].[Parts]
    ([CustomerId]);
GO

-- Creating foreign key on [EmployeeGroupId] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [FK_Employees_EmployeeGroups]
    FOREIGN KEY ([EmployeeGroupId])
    REFERENCES [dbo].[EmployeeGroups]
        ([EmployeeGroupId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Employees_EmployeeGroups'
CREATE INDEX [IX_FK_Employees_EmployeeGroups]
ON [dbo].[Employees]
    ([EmployeeGroupId]);
GO

-- Creating foreign key on [PersonId] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [FK_Employees_People]
    FOREIGN KEY ([PersonId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Employees_People'
CREATE INDEX [IX_FK_Employees_People]
ON [dbo].[Employees]
    ([PersonId]);
GO

-- Creating foreign key on [PartId] in table 'PartFixtures'
ALTER TABLE [dbo].[PartFixtures]
ADD CONSTRAINT [FK_PartFixtures_Parts]
    FOREIGN KEY ([PartId])
    REFERENCES [dbo].[Parts]
        ([PartId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PartFixtures_Parts'
CREATE INDEX [IX_FK_PartFixtures_Parts]
ON [dbo].[PartFixtures]
    ([PartId]);
GO

-- Creating foreign key on [PartId] in table 'PartVersions'
ALTER TABLE [dbo].[PartVersions]
ADD CONSTRAINT [FK_PartVersions_Parts]
    FOREIGN KEY ([PartId])
    REFERENCES [dbo].[Parts]
        ([PartId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PartVersions_Parts'
CREATE INDEX [IX_FK_PartVersions_Parts]
ON [dbo].[PartVersions]
    ([PartId]);
GO

-- Creating foreign key on [DocumentFolderId] in table 'Documents'
ALTER TABLE [dbo].[Documents]
ADD CONSTRAINT [FK_Documents_DocumentFolders]
    FOREIGN KEY ([DocumentFolderId])
    REFERENCES [dbo].[DocumentFolders]
        ([DocumentFolderId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Documents_DocumentFolders'
CREATE INDEX [IX_FK_Documents_DocumentFolders]
ON [dbo].[Documents]
    ([DocumentFolderId]);
GO

-- Creating foreign key on [DocumentTypeId] in table 'Documents'
ALTER TABLE [dbo].[Documents]
ADD CONSTRAINT [FK_Documents_DocumentTypes]
    FOREIGN KEY ([DocumentTypeId])
    REFERENCES [dbo].[DocumentTypes]
        ([DocumentTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Documents_DocumentTypes'
CREATE INDEX [IX_FK_Documents_DocumentTypes]
ON [dbo].[Documents]
    ([DocumentTypeId]);
GO

-- Creating foreign key on [DocumentId] in table 'DocumentVersions'
ALTER TABLE [dbo].[DocumentVersions]
ADD CONSTRAINT [FK_DocumentVersions_Documents]
    FOREIGN KEY ([DocumentId])
    REFERENCES [dbo].[Documents]
        ([DocumentId])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DocumentVersions_Documents'
CREATE INDEX [IX_FK_DocumentVersions_Documents]
ON [dbo].[DocumentVersions]
    ([DocumentId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------