//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace nGen.Data.EntityFramework.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Supplier
    {
        public Supplier()
        {
            this.SupplierContacts = new HashSet<SupplierContact>();
        }
    
        public int SupplierId { get; set; }
        public string Name { get; set; }
        public string Terms { get; set; }
        public bool IsApproved { get; set; }
        public byte[] NotesBytes { get; set; }
        public int SupplierTypeId { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    
        public virtual ICollection<SupplierContact> SupplierContacts { get; set; }
        public virtual SupplierType SupplierType { get; set; }
    }
}
