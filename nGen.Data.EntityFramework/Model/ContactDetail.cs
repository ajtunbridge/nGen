//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace nGen.Data.EntityFramework.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ContactDetail
    {
        public ContactDetail()
        {
            this.CustomerContactDetails = new HashSet<CustomerContactDetail>();
            this.PersonContactDetails = new HashSet<PersonContactDetail>();
        }
    
        public int ContactDetailId { get; set; }
        public string Value { get; set; }
        public int ContactDetailTypeId { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    
        public virtual ContactDetailType ContactDetailType { get; set; }
        public virtual ICollection<CustomerContactDetail> CustomerContactDetails { get; set; }
        public virtual ICollection<PersonContactDetail> PersonContactDetails { get; set; }
    }
}
