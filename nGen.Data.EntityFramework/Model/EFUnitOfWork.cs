﻿#region Using directives

using System;
using nGen.Data.EntityFramework.Repositories;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Model
{
    public sealed class EFUnitOfWork : IUnitOfWork
    {
        private EFClientSettingRepository _clientSettings;
        private EFCustomerRepository _customers;
        private EFDocumentFolderRepository _documentFolders;
        private EFDocumentVersionRepository _documentVersions;
        private EFDocumentRepository _documents;
        private nGenEntities _entities = new nGenEntities();
        private EFPartVersionRepository _partVersions;
        private EFPartRepository _parts;
        private EFPersonRepository _people;

        #region IUnitOfWork Members

        public void Dispose()
        {
            if (_entities != null) {
                _entities.Dispose();
            }

            GC.SuppressFinalize(this);
        }

        public IClientSettingRepository ClientSettings
        {
            get { return _clientSettings ?? (_clientSettings = new EFClientSettingRepository(_entities)); }
        }

        public ICustomerRepository Customers
        {
            get { return _customers ?? (_customers = new EFCustomerRepository(_entities)); }
        }

        public IDocumentRepository Documents
        {
            get { return _documents ?? (_documents = new EFDocumentRepository(_entities)); }
        }

        public IDocumentFolderRepository DocumentFolders
        {
            get { return _documentFolders ?? (_documentFolders = new EFDocumentFolderRepository(_entities)); }
        }

        public IDocumentVersionRepository DocumentVersions
        {
            get { return _documentVersions ?? (_documentVersions = new EFDocumentVersionRepository(_entities)); }
        }

        public IPartRepository Parts
        {
            get { return _parts ?? (_parts = new EFPartRepository(_entities)); }
        }

        public IPartVersionRepository PartVersions
        {
            get { return _partVersions ?? (_partVersions = new EFPartVersionRepository(_entities)); }
        }

        public IPersonRepository People
        {
            get { return _people ?? (_people = new EFPersonRepository(_entities)); }
        }

        public void Commit()
        {
            _entities.SaveChanges();
        }

        public void DiscardChanges()
        {
            _entities = new nGenEntities();

            _customers = null;
            _people = null;
        }

        #endregion

        ~EFUnitOfWork()
        {
            Dispose();
        }
    }
}