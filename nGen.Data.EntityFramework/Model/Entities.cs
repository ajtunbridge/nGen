﻿#region Using directives

using System;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Data.EntityFramework.Model
{
    public partial class Address : IAddress, IEquatable<Address>
    {
        #region IEquatable<Address> Members

        public bool Equals(Address other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return AddressId == other.AddressId;
        }

        #endregion

        public override int GetHashCode()
        {
            return AddressId;
        }

        public static bool operator ==(Address left, Address right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Address left, Address right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((Address) obj);
        }
    }

    public partial class AddressType : IAddressType, IEquatable<AddressType>
    {
        #region IEquatable<AddressType> Members

        public bool Equals(AddressType other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return AddressTypeId == other.AddressTypeId;
        }

        #endregion

        public override int GetHashCode()
        {
            return AddressTypeId;
        }

        public static bool operator ==(AddressType left, AddressType right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(AddressType left, AddressType right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((AddressType) obj);
        }
    }

    public partial class ClientSetting : IClientSetting, IEquatable<ClientSetting>
    {
        #region IEquatable<ClientSetting> Members

        public bool Equals(ClientSetting other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return ClientSettingId == other.ClientSettingId;
        }

        #endregion

        public override int GetHashCode()
        {
            return ClientSettingId;
        }

        public static bool operator ==(ClientSetting left, ClientSetting right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ClientSetting left, ClientSetting right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((ClientSetting) obj);
        }
    }

    public partial class Customer : ICustomer, IEquatable<Customer>
    {
        #region IEquatable<Customer> Members

        public bool Equals(Customer other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return string.Equals(Name, other.Name);
        }

        #endregion

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public static bool operator ==(Customer left, Customer right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Customer left, Customer right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((Customer) obj);
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public partial class Document : IDocument, IEquatable<Document>
    {
        #region IEquatable<Document> Members

        public bool Equals(Document other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return PartId == other.PartId && PartVersionId == other.PartVersionId && CustomerId == other.CustomerId &&
                   string.Equals(FileName, other.FileName);
        }

        #endregion

        public override int GetHashCode()
        {
            unchecked {
                int hashCode = PartId.GetHashCode();
                hashCode = (hashCode*397) ^ PartVersionId.GetHashCode();
                hashCode = (hashCode*397) ^ CustomerId.GetHashCode();
                hashCode = (hashCode*397) ^ (FileName != null ? FileName.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Document left, Document right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Document left, Document right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((Document) obj);
        }
    }

    public partial class DocumentFolder : IDocumentFolder, IEquatable<DocumentFolder>
    {
        #region IEquatable<DocumentFolder> Members

        public bool Equals(DocumentFolder other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return ParentFolderId == other.ParentFolderId && CustomerId == other.CustomerId && PartId == other.PartId &&
                   PartVersionId == other.PartVersionId && string.Equals(Name, other.Name);
        }

        #endregion

        public override int GetHashCode()
        {
            unchecked {
                int hashCode = ParentFolderId.GetHashCode();
                hashCode = (hashCode*397) ^ CustomerId.GetHashCode();
                hashCode = (hashCode*397) ^ PartId.GetHashCode();
                hashCode = (hashCode*397) ^ PartVersionId.GetHashCode();
                hashCode = (hashCode*397) ^ (Name != null ? Name.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(DocumentFolder left, DocumentFolder right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DocumentFolder left, DocumentFolder right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((DocumentFolder) obj);
        }
    }

    public partial class DocumentType : IDocumentType, IEquatable<DocumentType>
    {
        #region IEquatable<DocumentType> Members

        public bool Equals(DocumentType other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return string.Equals(Name, other.Name);
        }

        #endregion

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public static bool operator ==(DocumentType left, DocumentType right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DocumentType left, DocumentType right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((DocumentType) obj);
        }
    }

    public partial class DocumentVersion : IDocumentVersion, IEquatable<DocumentVersion>
    {
        #region IDocumentVersion Members

        [Obsolete("Here because IEntity requires it but it's not required for the database")]
        public DateTime ModifiedOn { get; set; }

        [Obsolete("Here because IEntity requires it but it's not required for the database")]
        public int ModifiedBy { get; set; }

        #endregion

        #region IEquatable<DocumentVersion> Members

        public bool Equals(DocumentVersion other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return DocumentVersionId == other.DocumentVersionId;
        }

        #endregion

        public override int GetHashCode()
        {
            return DocumentVersionId;
        }

        public static bool operator ==(DocumentVersion left, DocumentVersion right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DocumentVersion left, DocumentVersion right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((DocumentVersion) obj);
        }
    }

    public partial class Employee : IEmployee, IEquatable<Employee>
    {
        #region IEquatable<Employee> Members

        public bool Equals(Employee other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return string.Equals(UserName, other.UserName);
        }

        #endregion

        public override int GetHashCode()
        {
            return (UserName != null ? UserName.GetHashCode() : 0);
        }

        public static bool operator ==(Employee left, Employee right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Employee left, Employee right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((Employee) obj);
        }
    }

    public partial class EmployeeGroup : IEmployeeGroup, IEquatable<EmployeeGroup>
    {
        #region IEquatable<EmployeeGroup> Members

        public bool Equals(EmployeeGroup other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return string.Equals(Name, other.Name);
        }

        #endregion

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public static bool operator ==(EmployeeGroup left, EmployeeGroup right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(EmployeeGroup left, EmployeeGroup right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((EmployeeGroup) obj);
        }
    }

    public partial class Part : IPart, IEquatable<Part>
    {
        #region IEquatable<Part> Members

        public bool Equals(Part other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return string.Equals(DrawingNumber, other.DrawingNumber) && CustomerId == other.CustomerId;
        }

        #endregion

        public override int GetHashCode()
        {
            unchecked {
                return ((DrawingNumber != null ? DrawingNumber.GetHashCode() : 0)*397) ^ CustomerId;
            }
        }

        public static bool operator ==(Part left, Part right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Part left, Part right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return DrawingNumber;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((Part) obj);
        }
    }

    public partial class PartVersion : IPartVersion, IEquatable<PartVersion>
    {
        #region IEquatable<PartVersion> Members

        public bool Equals(PartVersion other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return string.Equals(VersionNumber, other.VersionNumber) && PartId == other.PartId;
        }

        #endregion

        public override int GetHashCode()
        {
            unchecked {
                return ((VersionNumber != null ? VersionNumber.GetHashCode() : 0)*397) ^ PartId;
            }
        }

        public static bool operator ==(PartVersion left, PartVersion right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PartVersion left, PartVersion right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((PartVersion) obj);
        }
    }

    public partial class Person : IPerson, IEquatable<Person>
    {
        #region IEquatable<Person> Members

        public bool Equals(Person other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return PersonId == other.PersonId;
        }

        #endregion

        public override int GetHashCode()
        {
            return PersonId;
        }

        public static bool operator ==(Person left, Person right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Person left, Person right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((Person) obj);
        }
    }
}