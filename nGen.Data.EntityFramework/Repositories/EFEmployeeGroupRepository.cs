﻿#region Using directives

using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public sealed class EFEmployeeGroupRepository : EFRepository, IEmployeeGroupRepository
    {
        public EFEmployeeGroupRepository(nGenEntities entities) : base(entities)
        {
        }

        #region IEmployeeGroupRepository Members

        public IEmployeeGroup GetByPK(int id)
        {
            var key = "EmployeeGroup:PK:" + id;

            return (IEmployeeGroup)
                RetrieveFromOrAddToCache(key,
                    () => Entities.EmployeeGroups.Single(grp => grp.EmployeeGroupId == id));
        }

        public void Add(IEmployeeGroup entity)
        {
            CheckEntity<EmployeeGroup>(entity);

            Entities.EmployeeGroups.Add(entity as EmployeeGroup);
        }

        public void Update(IEmployeeGroup entity)
        {
            CheckEntity<EmployeeGroup>(entity);

            var existing = Entities.EmployeeGroups.Single(grp => grp.EmployeeGroupId == entity.EmployeeGroupId);

            Entities.Entry(existing).CurrentValues.SetValues(entity);
        }

        public void Delete(IEmployeeGroup entity)
        {
            CheckEntity<EmployeeGroup>(entity);

            Entities.EmployeeGroups.Remove(entity as EmployeeGroup);
        }

        public ICollection<IEmployeeGroup> FilterByName(string searchPattern)
        {
            var key = "EmployeeGroupCollection:Name:" + searchPattern;

            return (ICollection<IEmployeeGroup>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.EmployeeGroups
                        .Where(grp => SqlFunctions.PatIndex(searchPattern, grp.Name) > 0)
                        .Cast<IEmployeeGroup>()
                        .ToList());
        }

        #endregion
    }
}