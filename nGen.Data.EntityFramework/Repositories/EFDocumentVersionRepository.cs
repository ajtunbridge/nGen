﻿#region Using directives

using System.Collections.Generic;
using System.Linq;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public sealed class EFDocumentVersionRepository : EFRepository, IDocumentVersionRepository
    {
        public EFDocumentVersionRepository(nGenEntities entities) : base(entities)
        {
        }

        #region IDocumentVersionRepository Members

        public IDocumentVersion GetByPK(int id)
        {
            var key = "DocumentVersion:PK:" + id;

            return (IDocumentVersion)
                RetrieveFromOrAddToCache(key,
                    () => Entities.DocumentVersions.Single(dv => dv.DocumentVersionId == id));
        }

        public void Add(IDocumentVersion entity)
        {
            CheckEntity<DocumentVersion>(entity);

            Entities.DocumentVersions.Add(entity as DocumentVersion);
        }

        public void Update(IDocumentVersion entity)
        {
            CheckEntity<DocumentVersion>(entity);

            var existing = GetByPK(entity.DocumentVersionId);

            Entities.Entry(existing).CurrentValues.SetValues(entity);
        }

        public void Delete(IDocumentVersion entity)
        {
            CheckEntity<DocumentVersion>(entity);

            var existing = GetByPK(entity.DocumentVersionId);

            Entities.DocumentVersions.Remove(existing as DocumentVersion);
        }

        public IDocumentVersion GetLatestVersion(IDocument document)
        {
            // TODO: is this correct? no caching done here as we always want the latest version
            return Entities.DocumentVersions
                .Where(dv => dv.DocumentId == document.DocumentId)
                .OrderByDescending(dv => dv.CreatedOn)
                .FirstOrDefault();
        }

        public ICollection<IDocumentVersion> GetAllVersions(IDocument document)
        {
            var key = "DocumentVersionCollection:DocumentId:" + document.DocumentId;

            return (ICollection<IDocumentVersion>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.DocumentVersions
                        .Where(dv => dv.DocumentId == document.DocumentId)
                        .ToList()
                        .Cast<IDocumentVersion>()
                        .ToList());
        }

        #endregion
    }
}