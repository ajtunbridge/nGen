﻿#region Using directives

using System;
using System.Runtime.Caching;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public abstract class EFRepository
    {
        private static readonly MemoryCache _cache = new MemoryCache("EntityCache");

        private static readonly CacheItemPolicy _cachePolicy = new CacheItemPolicy {
            SlidingExpiration = new TimeSpan(0, 0, 30)
        };

        protected readonly nGenEntities Entities;

        protected EFRepository(nGenEntities entities)
        {
            Entities = entities;
        }

        /// <summary>
        /// Checks if an object with the given key is already in the cache and returns it if it is. If not, retrieves the
        /// object from the database using the supplied function and adds to the cache.
        /// </summary>
        /// <param name="key">The key of the object to retrieve/add</param>
        /// <param name="databaseCall">The function to call if the object doesn't exist in the cache and must be retrieved from the database</param>
        /// <returns></returns>
        protected object RetrieveFromOrAddToCache(string key, Func<object> databaseCall)
        {
            var cached = _cache.Get(key);

            if (cached != null) {
                return cached;
            }

            var result = databaseCall();

            if (result == null) {
                return null;
            }

            _cache.Add(key, result, _cachePolicy);
            
            return result;
        }

        /// <summary>
        ///     Checks the entity is null or of the incorrect type and throws an exception if it is
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <exception cref="ArgumentNullException">Thrown when the entity is null</exception>
        /// <exception cref="ArgumentException">Thrown when the entity is not of type <typeparamref name="T" /></exception>
        protected void CheckEntity<T>(IEntity entity) where T : IEntity
        {
            if (entity == null) {
                throw new ArgumentException("entity cannot be null", "entity");
            }

            if (!(entity is T)) {
                throw new ArgumentException("entity must be of type " + typeof (T).FullName, "entity");
            }
        }
    }
}