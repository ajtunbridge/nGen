﻿#region Using directives

using System.Collections.Generic;
using System.Linq;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public sealed class EFDocumentFolderRepository : EFRepository, IDocumentFolderRepository
    {
        public EFDocumentFolderRepository(nGenEntities entities) : base(entities)
        {
        }

        #region IDocumentFolderRepository Members

        public IDocumentFolder GetByPK(int id)
        {
            string key = "DocumentFolder:PK:" + id;

            return (IDocumentFolder)
                RetrieveFromOrAddToCache(key, () => Entities.DocumentFolders.Single(df => df.DocumentFolderId == id));
        }

        public void Add(IDocumentFolder entity)
        {
            CheckEntity<DocumentFolder>(entity);

            Entities.DocumentFolders.Add(entity as DocumentFolder);
        }

        public void Update(IDocumentFolder entity)
        {
            CheckEntity<DocumentFolder>(entity);

            IDocumentFolder existing = GetByPK(entity.DocumentFolderId);

            Entities.Entry(existing).CurrentValues.SetValues(entity);
        }

        public void Delete(IDocumentFolder entity)
        {
            CheckEntity<DocumentFolder>(entity);

            IDocumentFolder existing = GetByPK(entity.DocumentFolderId);

            Entities.DocumentFolders.Remove(existing as DocumentFolder);
        }

        public ICollection<IDocumentFolder> GetChildFolders(IDocumentFolder parentFolder)
        {
            string key = "DocumentFolderCollection:ParentFolder:" + parentFolder.ParentFolderId;

            return (ICollection<IDocumentFolder>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.DocumentFolders.Where(df => df.ParentFolderId == parentFolder.DocumentFolderId)
                        .ToList()
                        .Cast<IDocumentFolder>()
                        .ToList());
        }

        public ICollection<IDocumentFolder> GetCustomerFolders(ICustomer customer)
        {
            string key = "DocumentFolderCollection:Customer:" + customer.CustomerId;

            return (ICollection<IDocumentFolder>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.DocumentFolders.Where(df => df.CustomerId == customer.CustomerId)
                        .ToList()
                        .Cast<IDocumentFolder>()
                        .ToList());
        }

        public ICollection<IDocumentFolder> GetPartFolders(IPart part)
        {
            string key = "DocumentFolderCollection:Part:" + part.PartId;

            return (ICollection<IDocumentFolder>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.DocumentFolders.Where(df => df.PartId == part.PartId)
                        .ToList()
                        .Cast<IDocumentFolder>()
                        .ToList());
        }

        public ICollection<IDocumentFolder> GetPartVersionFolders(IPartVersion partVersion)
        {
            string key = "DocumentFolderCollection:PartVersion:" + partVersion.PartVersionId;

            return (ICollection<IDocumentFolder>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.DocumentFolders.Where(df => df.PartVersionId == partVersion.PartVersionId)
                        .ToList()
                        .Cast<IDocumentFolder>()
                        .ToList());
        }

        #endregion
    }
}