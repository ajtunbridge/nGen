﻿#region Using directives

using System.Collections.Generic;
using System.Linq;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public class EFPartRepository : EFRepository, IPartRepository
    {
        public EFPartRepository(nGenEntities entities) : base(entities)
        {
        }

        #region IPartRepository Members

        public IPart GetByPK(int id)
        {
            string key = "Part:PK:" + id;

            return (IPart)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Parts.Single(p => p.PartId == id));
        }

        public void Add(IPart entity)
        {
            CheckEntity<Part>(entity);

            Entities.Parts.Add(entity as Part);
        }

        public void Update(IPart entity)
        {
            CheckEntity<Part>(entity);

            var existing = GetByPK(entity.PartId);

            Entities.Entry(existing).CurrentValues.SetValues(entity);
        }

        public void Delete(IPart entity)
        {
            CheckEntity<Part>(entity);

            var existing = GetByPK(entity.PartId) as Part;

            Entities.Parts.Remove(existing);
        }

        public ICollection<IPart> GetCustomerParts(ICustomer customer)
        {
            return GetCustomerParts(customer.CustomerId);
        }

        public ICollection<IPart> GetCustomerParts(int customerId)
        {
            var key = "PartCollection:CustomerId:" + customerId;

            return (ICollection<IPart>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Parts.Where(p => p.CustomerId == customerId).ToList().Cast<IPart>().ToList());
        }

        #endregion
    }
}