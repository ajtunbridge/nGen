﻿#region Using directives

using System.Collections.Generic;
using System.Linq;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public sealed class EFPartVersionRepository : EFRepository, IPartVersionRepository
    {
        public EFPartVersionRepository(nGenEntities entities) : base(entities)
        {
        }

        #region IPartVersionRepository Members

        public IPartVersion GetByPK(int id)
        {
            string key = "PartVersion:PK:" + id;

            return (IPartVersion)
                RetrieveFromOrAddToCache(key,
                    () => Entities.PartVersions.Single(pv => pv.PartVersionId == id));
        }

        public void Add(IPartVersion entity)
        {
            CheckEntity<PartVersion>(entity);

            Entities.PartVersions.Add(entity as PartVersion);
        }

        public void Update(IPartVersion entity)
        {
            CheckEntity<PartVersion>(entity);

            var existing = Entities.PartVersions.Single(pv => pv.PartVersionId == entity.PartVersionId);

            Entities.Entry(existing).CurrentValues.SetValues(entity);
        }

        public void Delete(IPartVersion entity)
        {
            CheckEntity<PartVersion>(entity);

            var existing = Entities.PartVersions.Single(pv => pv.PartVersionId == entity.PartVersionId);

            Entities.PartVersions.Remove(existing);
        }

        public ICollection<IPartVersion> GetPartVersions(IPart part)
        {
            var key = "PartVersionCollection:PartId:" + part.PartId;

            return (ICollection<IPartVersion>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.PartVersions.Where(pv => pv.PartId == part.PartId).Cast<IPartVersion>().ToList());
        }

        #endregion
    }
}