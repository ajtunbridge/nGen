﻿#region Using directives

using System.Collections.Generic;
using System.Linq;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public sealed class EFDocumentRepository : EFRepository, IDocumentRepository
    {
        public EFDocumentRepository(nGenEntities entities) : base(entities)
        {
        }

        #region IDocumentRepository Members

        public IDocument GetByPK(int id)
        {
            var key = "Document:PK:" + id;

            return (IDocument)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Documents.Single(d => d.DocumentId == id));
        }

        public void Add(IDocument entity)
        {
            Entities.Documents.Add(entity as Document);
        }

        public void Update(IDocument entity)
        {
            var existing = Entities.Documents.Single(d => d.DocumentId == entity.DocumentId);

            Entities.Entry(existing).CurrentValues.SetValues(entity);
        }

        public void Delete(IDocument entity)
        {
            var existing = Entities.Documents.Single(d => d.DocumentId == entity.DocumentId);

            Entities.Documents.Remove(existing);
        }

        public ICollection<IDocument> GetCustomerDocuments(ICustomer customer)
        {
            var key = "DocumentCollection:CustomerId:" + customer.CustomerId;

            return (ICollection<IDocument>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Documents.Where(d => d.CustomerId == customer.CustomerId)
                        .ToList()
                        .Cast<IDocument>()
                        .ToList());
        }

        public ICollection<IDocument> GetFolderDocuments(IDocumentFolder documentFolder)
        {
            var key = "DocumentCollection:DocumentFolderId:" + documentFolder.DocumentFolderId;

            return (ICollection<IDocument>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Documents.Where(d => d.DocumentFolderId == documentFolder.DocumentFolderId)
                        .ToList()
                        .Cast<IDocument>()
                        .ToList());
        }

        public ICollection<IDocument> GetPartDocuments(IPart part)
        {
            var key = "DocumentCollection:PartId:" + part.PartId;

            return (ICollection<IDocument>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Documents.Where(d => d.PartId == part.PartId)
                        .ToList()
                        .Cast<IDocument>()
                        .ToList());
        }

        public ICollection<IDocument> GetPartVersionDocuments(IPartVersion partVersion)
        {
            var key = "DocumentCollection:PartVersionId:" + partVersion.PartVersionId;

            return (ICollection<IDocument>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Documents.Where(d => d.PartVersionId == partVersion.PartVersionId)
                        .ToList()
                        .Cast<IDocument>()
                        .ToList());
        }

        #endregion
    }
}