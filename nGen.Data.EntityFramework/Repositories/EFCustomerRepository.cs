﻿#region Using directives

using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public sealed class EFCustomerRepository : EFRepository, ICustomerRepository
    {
        public EFCustomerRepository(nGenEntities entities) : base(entities)
        {
        }

        #region ICustomerRepository Members

        public ICollection<ICustomer> FilterByName(string value)
        {
            var key = "CustomerCollection:Name:" + value;

            return (ICollection<ICustomer>) RetrieveFromOrAddToCache(key, () => Entities.Customers
                .Where(customer => SqlFunctions.PatIndex(value, customer.Name) > 0)
                .ToList()
                .Cast<ICustomer>()
                .ToList());
        }

        public ICollection<ICustomer> GetAll(bool approvedOnly)
        {
            var key = "CustomerCollection:IsApproved:" + approvedOnly;

            return (ICollection<ICustomer>) RetrieveFromOrAddToCache(key, () => {
                var customers = approvedOnly ? Entities.Customers.Where(c => c.IsApproved) : Entities.Customers;
                var cast = customers.ToList().Cast<ICustomer>().ToList();
                return cast;
            });
        }

        public ICustomer GetByPK(int id)
        {
            var key = "Customer:PK:" + id;

            return (ICustomer) RetrieveFromOrAddToCache(key, () => Entities.Customers.SingleOrDefault(c => c.CustomerId == id));
        }

        public void Add(ICustomer entity)
        {
            CheckEntity<Customer>(entity);

            Entities.Customers.Add(entity as Customer);
        }

        public void Update(ICustomer entity)
        {
            CheckEntity<Customer>(entity);

            var existing = GetByPK(entity.CustomerId);

            Entities.Entry(existing).CurrentValues.SetValues(entity);
        }

        public void Delete(ICustomer entity)
        {
            CheckEntity<Customer>(entity);

            var existing = GetByPK(entity.CustomerId);

            Entities.Customers.Remove(existing as Customer);
        }

        #endregion
    }
}