﻿#region Using directives

using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public class EFEmployeeRepository : EFRepository, IEmployeeRepository
    {
        public EFEmployeeRepository(nGenEntities entities) : base(entities)
        {
        }

        #region IEmployeeRepository Members

        public IEmployee GetByPK(int id)
        {
            string key = "Employee:PK:" + id;

            return (IEmployee)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Employees.Single(emp => emp.EmployeeId == id));
        }

        public void Add(IEmployee entity)
        {
            CheckEntity<Employee>(entity);

            Entities.Employees.Add(entity as Employee);
        }

        public void Update(IEmployee entity)
        {
            CheckEntity<Employee>(entity);

            IEmployee existing = GetByPK(entity.EmployeeId);

            Entities.Entry(existing).CurrentValues.SetValues(entity);
        }

        public void Delete(IEmployee entity)
        {
            CheckEntity<Employee>(entity);

            Entities.Employees.Remove(entity as Employee);
        }

        public IEmployee GetByUserName(string userName)
        {
            string key = "Employee:UserName:" + userName;

            return (IEmployee)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Employees.SingleOrDefault(
                        emp => emp.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase)));
        }

        public ICollection<IEmployee> FilterByFirstName(string query)
        {
            string key = "EmployeeCollection:FirstName:" + query;

            return (ICollection<IEmployee>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Employees
                        .Where(emp => SqlFunctions.PatIndex(string.Format("{0}%", query), emp.Person.FirstName) > 0)
                        .ToList()
                        .Cast<IEmployee>()
                        .ToList());
        }

        public ICollection<IEmployee> FilterByLastName(string query)
        {
            string key = "EmployeeCollection:LastName:" + query;

            return (ICollection<IEmployee>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Employees
                        .Where(emp => SqlFunctions.PatIndex(string.Format("{0}%", query), emp.Person.LastName) > 0)
                        .ToList()
                        .Cast<IEmployee>()
                        .ToList());
        }

        public ICollection<IEmployee> FilterByEmployeeGroup(IEmployeeGroup employeeGroup)
        {
            return FilterByEmployeeGroup(employeeGroup.EmployeeGroupId);
        }

        public ICollection<IEmployee> FilterByEmployeeGroup(int employeeGroupId)
        {
            string key = "EmployeeCollection:EmployeeGroup:" + employeeGroupId;

            return (ICollection<IEmployee>)
                RetrieveFromOrAddToCache(key,
                    () => Entities.Employees
                        .Where(emp => emp.EmployeeGroupId == employeeGroupId)
                        .ToList()
                        .Cast<IEmployee>()
                        .ToList());
        }

        #endregion
    }
}