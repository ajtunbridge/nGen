#region Using directives

using System.Linq;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public sealed class EFPersonRepository : EFRepository, IPersonRepository
    {
        public EFPersonRepository(nGenEntities entities) : base(entities)
        {
        }

        #region IPersonRepository Members

        public IPerson GetByPK(int id)
        {
            var key = "Person:PK:" + id;

            return (IPerson)
                RetrieveFromOrAddToCache(key,
                    () => Entities.People.Single(p => p.PersonId == id));
        }

        public void Add(IPerson entity)
        {
            CheckEntity<Person>(entity);

            Entities.People.Add(entity as Person);
        }

        public void Update(IPerson entity)
        {
            CheckEntity<Person>(entity);

            var existing = GetByPK(entity.PersonId);

            Entities.Entry(existing).CurrentValues.SetValues(entity);
        }

        public void Delete(IPerson entity)
        {
            CheckEntity<Person>(entity);

            var existing = GetByPK(entity.PersonId) as Person;

            Entities.People.Remove(existing);
        }

        #endregion
    }
}