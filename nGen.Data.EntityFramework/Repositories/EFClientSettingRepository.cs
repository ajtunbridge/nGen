﻿#region Using directives

using System.Linq;
using nGen.Data.EntityFramework.Model;
using nGen.Domain.Data.Model;
using nGen.Domain.Data.Repositories;

#endregion

namespace nGen.Data.EntityFramework.Repositories
{
    public sealed class EFClientSettingRepository : EFRepository, IClientSettingRepository
    {
        public EFClientSettingRepository(nGenEntities entities) : base(entities)
        {
        }

        #region IClientSettingRepository Members

        public IClientSetting GetByPK(int id)
        {
            return Entities.ClientSettings.Single(c => c.ClientSettingId == id);
        }

        public void Add(IClientSetting entity)
        {
            CheckEntity<ClientSetting>(entity);

            Entities.ClientSettings.Add(entity as ClientSetting);
        }

        public void Update(IClientSetting entity)
        {
            CheckEntity<ClientSetting>(entity);

            var existing = GetByPK(entity.ClientSettingId);

            Entities.Entry(existing).CurrentValues.SetValues(entity);
        }

        public void Delete(IClientSetting entity)
        {
            CheckEntity<ClientSetting>(entity);

            var existing = GetByPK(entity.ClientSettingId);

            Entities.ClientSettings.Remove(existing as ClientSetting);
        }

        public IClientSetting GetClientSettings()
        {
            return Entities.ClientSettings.First();
        }

        #endregion
    }
}